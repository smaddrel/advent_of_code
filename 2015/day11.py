"""Advent of Code Day 11: Corporate Policy"""
"""
This is going to be super reursive
Have a func to update each step
Need another wrapper for the position
Could it be a decorator?
"""
current_password = 'hepxcrrq'


def iterate_through(password, i, n):
    """Wrap the iteration to loop through steps."""
    location = n - i
    # print(location, 'location')
    if password[location] == 'z':
        # print("Got a z")
        password = password[:location] + 'a' + password[location+1:]
        password = iterate_through(password, i+1, n)  # <--- Recursion
    else:
        password = get_next_password(password, location)
    return password

def get_bytes_password(password):
    """Convert to bytes."""
    return [b for b in str.encode(password)]

def get_next_password(password, position):
    """Update the password based on position in string."""
    # Turn the string inot bytes
    bytes_password = get_bytes_password(password)
    # Set the letters to skip over
    void_letters = [b for b in str.encode('iol')]
    if bytes_password[position] < 97 + 25:
        bytes_password[position] += 1
        if bytes_password[position] in void_letters:
            bytes_password[position] += 1

    # print(bytes_password)
    return bytes(bytes_password).decode("utf-8")


def check_password(password):
    """Check the password meets the criteria."""
    b_password = get_bytes_password(password)
    sets_of_three = [b_password[i:i+3] for i in range(len(b_password) - 2)]

    sums_of_triptics = [True for trips in sets_of_three if
                        trips[1]-trips[0] == 1 and trips[2]-trips[1] == 1]
    if True in sums_of_triptics:
        pass1 = True
    else:
        pass1 = False
    pairs_bytes = []
    pairs = [b_password[i:i+2] for i in range(len(b_password) - 1)]

    pairs_bytes = [p[0] for p in pairs if p[0] == p[1]]
    if len(set(pairs_bytes)) >= 2:
        pass2 = True
    else:
        pass2 = False

    return pass1 and pass2


valid = False
check_password(current_password)
while valid is False:
    current_password = iterate_through(current_password, 1, 8)
    valid = check_password(current_password)
    if valid:
        print(current_password, 'password valid 1:', valid)
valid = False
while valid is False:
    current_password = iterate_through(current_password, 1, 8)
    valid = check_password(current_password)
    if valid:
        print(current_password, 'password valid 2:', valid)
