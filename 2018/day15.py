"""Advent of code: Day 15, combat."""

"""
(1) identify targets
(2) identify squares next to targets
(3) find cloest square to self - via path and get path
(4) split ties based on reading order
(5) move along path towards enemy
"""

class unit(object):
    """docstring for [object Object]."""
    def __init__(self, pos, species):
        self.x = pos[0]
        self.y = pos[1]  # x, y coords
        self.species = species
        self.path = None #list of numbers defineing the steps on the path

    def move(self, dir):
        """"Update the position."""
        if dir == 0:
            self.y -= 1
        if dir == 1:
            self.x -= 1
        if dir == 2:
            self.y += 1
        if dir == 3:
            self.x += 1




def print_grid(grid, units):
    print('\n')
    skip = False
    for y, row in enumerate(grid):
        for x, col in enumerate(row):
            skip = False
            for creature in units:
                if creature.x == x and creature.y == y:
                    marker = creature.species
                    print(marker, end='')
                    skip = True
            if skip is False:
                print(grid[y][x], end='')
        print('')



example_1 = ['#########',
             '#G..G..G#',
             '#.......#',
             '#.......#',
             '#G..E..G#',
             '#.......#',
             '#.......#',
             '#G..G..G#',
             '#########']

grid = example_1
grid = [list(row) for row in grid]
units = []
for y, row in enumerate(grid):
    for x, col in enumerate(row):
        if example_1[y][x] in 'GE':
            units.append(unit((x, y), grid[y][x]))
            grid[y][x] = '.'
print_grid(grid, units)
for creature in units:
    creature.move(2)
print_grid(grid, units)
