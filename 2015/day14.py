"""Advent of Code 2015: reindeer Races"""
import utils

total_time = 2503
# total_time = 1000

def get_test_fleet():
    """Get the test fleet from the problem to test."""
    deer = [{'resting_t': 127, 'lapt': 0, 'name': 'Comet',
            'flying_t': 10, 'speed': 14, 'distance': 0, 'state': 1,
            'score': 0},
            {'resting_t': 162, 'lapt': 0, 'name': 'Dancer',
             'flying_t': 11, 'speed': 16, 'distance': 0, 'state': 1,
             'score': 0}]
    return deer

def parse_line(line):
    """Parse the line and return the dict for the deer."""
    line = line.split()
    deer = {'state': 1, 'lapt': 0, 'distance': 0}
    deer['name'] = line[0]
    deer['speed'] = int(line[3])
    deer['flying_t'] = int(line[6])
    deer['resting_t'] = int(line[13])
    deer['score'] = 0
    return deer

def update_deer(deer):
    """Update the deer dictoinary."""
    deer['lapt'] += 1
    if deer['state'] == 1:
        deer['distance'] += deer['speed']
        if deer['lapt'] == deer['flying_t']:
            deer['state'] = 0
            deer['lapt'] = 0
    else:
        if deer['lapt'] == deer['resting_t']:
            deer['state'] = 1
            deer['lapt'] = 0
    return deer


def increase_score(deer):
    """increase the score and return the updated dict."""
    deer['score'] += 1
    return deer

def score_deer(fleet):
    """Score the leading deer."""
    fleet  = sorted(fleet, key=lambda k:k['distance'], reverse=True)
    current_lead = fleet[0]['distance']
    fleet = [increase_score(deer) if (deer['distance'] == current_lead) else deer for deer in fleet ]
    return fleet

fleet_info = utils.read_file('data14')

fleet = []
testing = False
if testing is False:
    for line in fleet_info:
        fleet.append(parse_line(line))
else:
    fleet = get_test_fleet()

for t in range(total_time+1):
    fleet = [update_deer(deer) for deer in fleet]
    fleet = score_deer(fleet)
    # print(fleet[0])
sorted_fleet = sorted(fleet, key=lambda k:k['distance'], reverse=True)
scored_fleet = sorted(fleet, key=lambda k:k['score'], reverse=True)
print('Furthest Distance:\n',sorted_fleet[0], '\n\n')
print('Highest Score:\n', scored_fleet[0], '\n\n')
