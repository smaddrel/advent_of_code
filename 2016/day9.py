"""advent of code day 9 2016 - Explosive Cyberspace."""
from utils import read_file

"""
Decompress strings based on marker rules
"""

def parse_code(code, part=1):
    # get the marker and copy, recursively if part 2
    update_string = []
    i = 0
    while i < len(code):
        if code[i] == '(':
            j = 0
            while code[i+j] != ')':
                j += 1
            marker = code[i:i+j+1]
            marker = marker[1:-1]
            marker = [int(x) for x in marker.split('x')]
            # Extract the copy snipit
            copy = code[i + j + 1: i + j + 1 + marker[0]]
            for _ in range(marker[1]):
                update_string.append(copy)
            i = i + j + 1 + marker[0]
        else:
            update_string.append(code[i])
            i += 1
    final_string = "".join(c for c in update_string)
    """
    For part 2:
    """
    if '(' in final_string and part == 2:
        print("Going deeper...")
        parse_code(final_string, part=part)
    return final_string

test_strings = ['A(2x2)BCD(2x2)EFG', 'A(1x5)BC', '(3x3)XYZ',
                '(6x1)(1x3)A', 'X(8x2)(3x3)ABCY',
                '(27x12)(20x12)(13x14)(7x10)(1x12)A']

input_strings = read_file('data9')

TEST = True
if TEST:
    coded_strings = test_strings
else:
    coded_strings = input_strings


print("===== PART 1 =====")
for code in coded_strings:
    final_string = parse_code(code)
    print("The final string: ", final_string[:10], " ... ----> Length: ",
          len(final_string))
print("===== PART 2 =====")
for code in coded_strings:
    final_string = parse_code(code, part=2)
    print("The final string: ", final_string[:10], " ... ----> Length: ",
          len(final_string))
