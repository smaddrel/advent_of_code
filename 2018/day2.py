"""Advent of Code 2018 - Day 2: Checksum, matching IDs."""
from time import sleep

def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.readlines()
        lines = [line.strip('\n') for line in lines]
    return lines

def count_chars(line):
    done = {}
    for c in line:
        if c not in done:
            num = line.count(c)
            done[c] = num
    return done

def filter_dict(counter):
    two = 0
    three = 0
    for key in counter:
        if counter[key] == 2:
            two = 1
        if counter[key] == 3:
            three = 1
    return two, three

def id_compare(lines):
    for i, id1 in enumerate(lines):
        for id2 in lines[:i] + lines[i+1:]:
            def inner_loop(id1, id2):
                count = 0
                for index in range(len(id1)):
                    if id1[index] != id2[index]:
                        count += 1
                    if count > 1:
                        return None

                return True

            flag = inner_loop(id1, id2)
            if flag is not None:
                print("Found only one diff!")
                print(id1, id2, flag)
                return



lines = read_file('day2')
tracking_sum = {'two': 0, 'three': 0}
for line in lines:
    counter = count_chars(line)
    check = filter_dict(counter)
    tracking_sum['two'] += check[0]
    tracking_sum['three'] += check[1]
print('Checksum: ', tracking_sum['two']*tracking_sum['three'])
id_compare(lines)
