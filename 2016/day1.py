"""Day 1 - Advent of Code 2016 - Distance on a grid."""
from utils import read_file
import logging

logging.basicConfig(level=logging.DEBUG)

input = read_file('data1')[0]
# logging.info(input)

# String to list of steps
input = input.replace(' ', '')
input = input.split(',')
# steps = [step for step in input]
# logging.info(input)

# -------------------
X = 0  # The left/right counter
Y = 0  # The up / down counter

def turn(direction, turn):
    """Take direction, turn, return direction."""
    # North
    if direction == 'N':
        if turn == 'R':
            direction = 'E'
        if turn == 'L':
            direction = 'W'
        return direction
    # East
    if direction == 'E':
        if turn == 'L':
            direction = 'N'
        if turn == 'R':
            direction = 'S'
        return direction
    # South
    if direction == 'S':
        if turn == 'L':
            direction = 'E'
        if turn == 'R':
            direction = 'W'
        return direction
    # West
    if direction == 'W':
        if turn == 'L':
            direction = 'S'
        if turn == 'R':
            direction = 'N'
        return direction

def parse_step(direction, step):
    """Parse the step and return the x / y shift."""
    if turn(direction, step[0]) == 'S':
        x = -1*int(step[1:])
        y = 0
    if turn(direction, step[0]) == 'N':
        x = 1*int(step[1:])
        y = 0
    if turn(direction, step[0]) == 'E':
        y = 1*int(step[1:])
        x = 0
    if turn(direction, step[0]) == 'W':
        y = -1*int(step[1:])
        x = 0
    return (turn(direction, step[0]), x, y)

def check_position(tup, position_log):
    if tup in position_log:
        logging.info("At the first double location. {}".format((X, Y)))
        logging.info("Total number of blocks: {}".format(abs(X) + abs(Y)))
        return 1
    return 0

direction = 'N'
# input=['R8', 'R4', 'R4', 'R8']
position_log = []
flag = 0
while flag is 0:
    for step in input:
        # logging.info("Step: {}".format(step))
        (direction, x, y) = parse_step(direction, step)
        print(x, y)
        if abs(x) > 0:
            for x_ in range(abs(x)):
                if x > 0:
                    X += 1
                else:
                    X -= 1
                # print(X)
                flag = check_position((X, Y), position_log)
                if flag == 1:
                    break
                logging.debug(flag)
                position_log.append((X, Y))

        if abs(y) > 0:
            for y_ in range(abs(y)):
                if y > 0:
                    Y += 1
                else:
                    Y -= 1
                # print(Y)
                flag = check_position((X, Y), position_log)
                if flag == 1:
                    break
                logging.debug(flag)
                position_log.append((X, Y))
        if flag == 1:
            break

logging.info("Total number of blocks: {}".format(abs(X) + abs(Y)))
