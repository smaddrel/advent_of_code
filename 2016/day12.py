from utils import read_file

input = read_file('data12')

def try_int(value):
    """Try / Except for the integers."""
    try:
        value = int(value)
        return value
    except ValueError:
        return value

def process_data(line):
    """Parse the text input line."""
    line = line.split(' ')
    return [try_int(value) for value in line]


parsed = [process_data(line) for line in input]


class assembly_instructions(object):
    """docstring for assembly_instructions."""

    def __init__(self):
        super(assembly_instructions, self).__init__()
        # Init the register default values
        self.a = 0
        self.b = 0
        self.c = 0
        self.d = 0

    def cpy(self, value, register):
        """Copy the value into the register arg --> class variable."""
        # print("value", value, "register", register)
        try:
            int(value)
            setattr(self, register, value)
        except ValueError:
            # print(register, getattr(self, value))
            setattr(self, register, getattr(self, value))

        return 1

    def inc(self, register):
        """Increase register by 1."""
        setattr(self, register, getattr(self, register) + 1)
        return 1

    def dec(self, register):
        """Decrease register by 1."""
        # print(getattr(self, register))
        setattr(self, register, getattr(self, register) - 1)
        return 1

    def jnz(self, register, skip):
        """Skip the number of instructions, check is int or string first."""
        try:
            int(register)
            if register != 0:
                return skip
            else:
                return 1
        except ValueError:
            if getattr(self, register) != 0:
                return skip
            else:
                return 1


    def _print(self):
        """Print the register values."""
        # print(f"a = {self.a}, b = {self.b}, c = {self.c}, d = {self.d}")
        print("a = {0}, b = {1}, c = {2}, d = {3}".
              format(self.a, self.b, self.c, self.d))

"""
# Part 1:
print("starting part 1...")
assem = assembly_instructions()
counter = 0
while counter < len(parsed):
    # print(parsed[counter])
    args = (arg for arg in parsed[counter][1:])
    incriment = getattr(assem, parsed[counter][0])(*args)
    # print(incriment)
    # assem._print()
    # print("Counter:", counter, incriment, counter + incriment)
    counter += incriment
print("final values.")
assem._print()
"""
# Part 2:
assem = assembly_instructions()
assem.c = 1
counter = 0
print("starting part 2 ... ")
while counter < len(parsed):
    # print(parsed[counter])
    args = (arg for arg in parsed[counter][1:])
    incriment = getattr(assem, parsed[counter][0])(*args)
    # print(incriment)
    # assem._print()
    # print("Counter:", counter, incriment, counter + incriment)
    counter += incriment
print("Final values")
assem._print()
