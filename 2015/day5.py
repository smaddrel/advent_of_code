"""Advent of Code Day 5: Naughty or Nice."""
import utils

data = utils.read_file('data5')

# Part I: The old rules
good_list_counter = 0
vowels = ['a','e','i','o','u']
banned_list = ['ab', 'cd', 'pq','xy']
for line in data:
    validate = {'vowels': 0,
                'double': False,
                'banned': False}
    line = '0'+line+'0'
    for i in range(1, len(line)):
        if line[i] in vowels:
            validate['vowels'] += 1
        if str(line[i - 1: i + 1]) in banned_list:
            validate['banned'] = True
        if line[i - 1] == line[i]:
            validate['double'] = True
    # Check the validation to find good lists
    if (validate['vowels'] >= 3) and (validate['double'] is True) and (validate['banned'] is False):
        good_list_counter += 1
print('Original good list count:', good_list_counter)

# Part II: The new rules
good_list_counter = 0
# data = ['qjhvhtzxzqqjkmpb']
# data = ['xxyxx']
# data = ['uurcxstgmygtbstg']
# data = ['ieodomkazucvgmuy']
# All tests are correct
for line in data:
    validate = {'doubles': [],
                'last_double': 0,
                'split_pair': False,
                'pair_doubles': False}
    line = '01'+line+'10'
    for i in range(2, len(line)):
        if line[i - 2] == line[i]:
            validate['split_pair'] = True
        if (str(line[i-1:i+1]) in validate['doubles']) and (validate['last_double'] < i -1):
            validate['pair_doubles'] = True
            last_double = i
        validate['doubles'].append(str(line[i-1:i+1]))
    if (validate['split_pair'] is True) and (validate['pair_doubles'] is True):
        good_list_counter += 1
print('Updated good list count:', good_list_counter)
