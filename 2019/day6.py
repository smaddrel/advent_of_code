"""Advent of Code 2019 - Day 6 - Orbits as Graph theory."""

test_input = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L"
test_input = test_input.split('\n')

# Load input
with open('data/data6.txt') as f:
    puzzle_input = [wire.strip('\n') for wire in f.readlines()]



class graph(object):
    """docstring for graph."""

    def __init__(self):
        self.out_edges = {}
        self.in_edges = {}

    def force_bidirectional(self):
        """Update out edges with all in edges -> bi-directional"""
        for node in self.in_edges.keys():
            for in_edge in self.in_edges[node]:
                self.out_edges[node].append(in_edge)
        # print(self.out_edges)

    def connect_outputs(self, nodes):
        """Connect the flow from node -> outputs."""
        """
            Inputs: nodes - list of pair of nodes
                          - assumed node[0] -> node[1]

            Take the first arg of nodes and look for all outputs from that node
        """
        # use the fact that empty dict {} evaluates to False in python
        if self.out_edges:
            # If node already found then append to the dict list
            if nodes[0] in self.out_edges.keys():
                self.out_edges[nodes[0]].append(nodes[1])
            else:
                # Else add a new entry to the dictionary
                self.out_edges[nodes[0]] = [nodes[1]]
        else:
            # If the dictionary is empty add first entry here
            self.out_edges[nodes[0]] = [nodes[1]]

    def connect_inputs(self, nodes):
        """Connect the flow from inputs -> node."""
        """
            Inputs: nodes - list of pair of nodes
                          - assumed node[0] -> node[1]

            Take the second arg of nodes and look for all inputs to that node
        """
        # use the fact that empty dict {} evaluates to False in python
        if self.in_edges:
            # If node already found then append to the dict list
            if nodes[1] in self.in_edges.keys():
                self.in_edges[nodes[1]].append(nodes[0])
            else:
                # Else add a new entry to the dictionary
                self.in_edges[nodes[1]] = [nodes[0]]
        else:
            # If the dictionary is empty add first entry here
            self.in_edges[nodes[1]] = [nodes[0]]

    def check_hanging_nodes(self):
        """Make sure i/o for all nodes are full populated."""
        # Unpack the keys into lists + combine with set to remove duplicates
        # Mostly not important but catches single edge and floating nodes
        all_nodes = set([*self.in_edges.keys()] + [*self.out_edges.keys()])
        # Then add empty list for each node to the dict
        # These are mostly populated already
        for node in all_nodes:
            if node not in self.in_edges.keys():
                self.in_edges[node] = []
            if node not in self.out_edges.keys():
                self.out_edges[node] = []
        # Create a list with all nodes
        self.nodes = list(all_nodes)


def breadth_first_search(Graph, current_node=None, target_node=None):
    """Find all paths of given length from the starting node."""
    """
        Inputs:
            Graph - The Graph class, containing adjecent information
            current_node - Can pass in a specified current node
            target_node - Specifying a taget node turns into a proper search
        Returns:
            running_total - the number of edges in the set of paths
    """
    # Get a starting node and update current state
    if current_node is None:
        # Get a random start point on the graph
        current_node = random.choice(Graph.nodes)
    # Init the conditions for each BFS
    Graph.current_node = current_node
    Graph.visited = [current_node]
    Graph.all_paths = []
    # Unique paths list
    paths = [[current_node]]
    running_total = 0
    # while there are elements in the path  search neighbours
    while paths:
        current_path = paths.pop(0)
        Graph.current_node = current_path[-1]
        if Graph.current_node == target_node:
            print("Foundpath from {} to {}!".format(Graph.visited[0], target_node))
            return current_path

        if len(Graph.out_edges[Graph.current_node]) == 0:
            # Found a deadend that can't be escaped
            # If all paths are deadends they all get popped from the list
            # before new paths can be added leaving an empty list.
            # Where paths evalutes to false, exiting the loop.
            Graph.all_paths.append(current_path)
            continue

        # Loop through the possible next steps in the graph
        for node in Graph.out_edges[Graph.current_node]:
            if target_node is not None and node in Graph.visited:
                # When finidng a specific path this is needed to avoid loops
                # More generally just required for bi-drectional path finding
                continue
            # Create a copy of the current path.
            new_path = list(current_path)
            # add each node option
            new_path.append(node)
            running_total += len(new_path) - 1
            # Re-add the path with the updated end node to the list.
            paths.append(new_path)
            # Typically would want to avoid looping back, not for random walk
            Graph.visited.append(node)

    return running_total


def process_line(line):
    """Split the line into two nodes to return."""
    nodes = line.split(')')
    return nodes

Orbits = graph()
# INPUT = test_input
INPUT = puzzle_input
for line in INPUT:
    nodes = process_line(line)
    Orbits.connect_outputs(nodes)
    Orbits.connect_inputs(nodes)
Orbits.check_hanging_nodes()

total_edges = breadth_first_search(Orbits, current_node='COM')
print("Part1, total number of orbits: ", total_edges)

# Part 2
# From Node YOU to node SANTA
test_input = 'COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN'
test_input = test_input.split('\n')

Orbits = graph()
# INPUT = test_input
INPUT = puzzle_input
for line in INPUT:
    nodes = process_line(line)
    Orbits.connect_outputs(nodes)
    Orbits.connect_inputs(nodes)
Orbits.check_hanging_nodes()
Orbits.force_bidirectional()
path = breadth_first_search(Orbits, current_node='YOU', target_node='SAN')
num_orbits = len(path) - 1
num_transferes = num_orbits - 2
print("Path length: ", num_transferes)
