"""Advent of Code Day 2 : 2015 - present area problem"""
def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.readlines()
        lines = [line.strip('\n') for line in lines]
    return lines

def ribbon_circ(l, w, h):
    """find the smallest circumfrence around the box."""
    slices = [2*l+2*h, 2*l+2*w, 2*w+2*h]
    smallest = min(slices)
    return smallest

def volume(l, w, h):
    """Calculate the volume of the box."""
    return l*w*h

def extra_bit(l, w, h):
    """The smallest extra face calculation."""
    return min([l*w, w*h, h*l])

def calculate_area(l, w, h):
    """Calcualte the area of the paper needed for the base box."""
    return 2*l*w + 2*l*h + 2*w*h

lines = read_file('data2')
total_area_needed = 0
ribbon_needed = 0
for line in lines:
    split_line = line.split('x')
    l = float(split_line[0])
    h = float(split_line[1])
    w = float(split_line[2])
    area = calculate_area(l, w, h)
    delta_area = extra_bit(l, w, h)
    length = ribbon_circ(l, w, h)
    vol = volume(l, w, h)
    total_ribbon = vol + length
    total_area = area + delta_area
    total_area_needed += total_area
    ribbon_needed += total_ribbon
print("Total area needed:", total_area_needed)
print("Total ribbon needed:", ribbon_needed)
#
# test
area = calculate_area(2, 3, 4)
delta_area = extra_bit(2, 3, 4)
length = ribbon_circ(2, 3, 4)
vol = volume(2, 3, 4)
total_ribbon = vol + length
total_area = area + delta_area
print(total_area)
print(total_ribbon)
area = calculate_area(1, 1, 10)
delta_area = extra_bit(1, 1, 10)
length = ribbon_circ(1, 1, 10)
vol = volume(1, 1, 10)
total_ribbon = vol + length
total_area = area + delta_area
print(total_area)
print(total_ribbon)
