"""Day 6: Grid of lights."""
import utils
import numpy as np
import matplotlib.pyplot as plt

def update_grid():
    """Read co-ordinates and update appropriately."""
    pass

def parse_line(line):
    """Parse the line to etxract the rectangle."""
    # return on/off/toggle
    # upper left
    # lower right
    line = line.replace('turn ', '')
    line = line.split(' ')
    upper_coords = [int(x) for x in line[1].split(',')]
    lower_coords = [int(x) for x in line[3].split(',')]
    all_lights = [[x, y] for x in range(upper_coords[0], lower_coords[0]+1)
                         for y in range(upper_coords[1], lower_coords[1]+1)]
    return (line[0], all_lights)


instructions = utils.read_file('data6')
# start with a grid of zeros 1000x1000, toggel 0 -> 1 for on/off respectively
base_grid = np.zeros(shape=(1000, 1000))
# instructions = ['turn on 0,0 through 999,999','turn off 0,0 through 499,499','toggle 250,250 through 749,749']
for line in instructions:
    ret = parse_line(line)
    if ret[0] == 'on':
        for coord in ret[1]:
            # Turn on
            base_grid[coord[0], coord[1]] = 1
    if ret[0] == 'off':
        for coord in ret[1]:
            # Turn off
            base_grid[coord[0], coord[1]] = 0
    if ret[0] == 'toggle':
        for coord in ret[1]:
            # Flip the lights
            if base_grid[coord[0], coord[1]] == 0:
                base_grid[coord[0], coord[1]] = 1
            elif base_grid[coord[0], coord[1]] == 1:
                base_grid[coord[0], coord[1]] = 0
print('Total number of lights on (part I):', int(np.sum(base_grid)))
plt.imshow(base_grid)
plt.show()
# Part II
base_grid = np.zeros(shape=(1000, 1000))
# instructions = ['turn on 0,0 through 999,999','turn off 0,0 through 499,499','toggle 250,250 through 749,749']
for line in instructions:
    ret = parse_line(line)
    if ret[0] == 'on':
        for coord in ret[1]:
            # Turn on
            base_grid[coord[0], coord[1]] += 1
    if ret[0] == 'off':
        for coord in ret[1]:
            # Turn off
            if base_grid[coord[0], coord[1]] >= 1:
                base_grid[coord[0], coord[1]] -= 1
    if ret[0] == 'toggle':
        for coord in ret[1]:
            # Flip the lights
            base_grid[coord[0], coord[1]] += 2
print('Total intensity of lights on: (part II)', int(np.sum(base_grid)))
plt.imshow(base_grid)
plt.show()
