"""Advent of Code: 2015 Day 12 - JSFramework."""
import utils
import re
import json

full_data = utils.read_file('data12')

def count_numbers(list_lines):
    """Count all the numbers from a list of lines."""
    numbers = [re.findall(r'-?\d+', line) for line in full_data]
    int_nums = [[int(n) for n in nums] for nums in numbers]
    sum_of_numbers = sum([sum(sublist) for sublist in int_nums])
    return sum_of_numbers

def remove_red_dicts(lines):
    """Remove any dicts that contain red keyword."""
    print(type(lines))
    if isinstance(lines, dict):
        if 'red' not in lines.values():
            print("recursion!")
            return [remove_red_dicts(d) for d in lines.items()]
    if isinstance(lines, list) or isinstance(lines, tuple):
        return [remove_red_dicts(d) for d in lines]


print('Sum of all numbers in doc: ', count_numbers(full_data))

# Part 2: Discount all numbers where we have red in a dict somewhere
reduced = remove_red_dicts(json.loads(full_data[0]))
# remove_red_dicts(json.loads(full_data))
