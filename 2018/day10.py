"""Advent of code: Day 10, moving messages."""
import matplotlib.pyplot as plt

def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.read().split("\n")
    return lines[:-1]

def update(X, Y, Dx, Dy):
    X_ret = []
    Y_ret = []
    for x, y, dx, dy in zip(X, Y, Dx, Dy):
        X_ret.append(x + dx)
        Y_ret.append(y + dy)
    return X_ret, Y_ret


lines = read_file('day10')
X = []
Y = []
dx = []
dy = []
for line in lines:
    line = line.replace('<', ',')
    line = line.replace('>', ',')
    line = line.split(',')
    X.append(int(line[1]))
    Y.append(int(line[2]))
    dx.append(int(line[4]))
    dy.append(int(line[5]))


for i in range(50750):
    X, Y = update(X, Y, dx, dy)
    if max(Y) - min(Y) < 11:
        if i % 1 == 0:
            plt.figure(figsize=(10,3))
            plt.scatter(X, Y, s=85, marker='s', )
            plt.savefig('plots/{}.png'.format(i))
            plt.close()
