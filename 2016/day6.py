"""Advent of Code 2016 - day6 - repeat code"""

from utils import read_file
import logging
from collections import Counter

logging.basicConfig(level=logging.DEBUG)

test = False

if test is False:
    input = read_file('data6')
    positions = [[],[],[],[],[],[],[],[]]
else:
    positions = [[],[],[],[],[],[]]
    input = ['eedadn', 'drvtee', 'eandsr', 'raavrd', 'atevrs', 'tsrnev', 'sdttsa',
             'rasrtv', 'nssdts', 'ntnada', 'svetve', 'tesnvt', 'vntsnd', 'vrdear',
             'dvrsen', 'enarar']


for scramble in input:
    for i, c in enumerate(scramble):
        positions[i].append(c)

word = [Counter(posi).most_common(1)[0][0] for posi in positions]
word = ''.join(c for c in word)
logging.info("The unscrambled word: {}".format(word))


word = [Counter(posi).most_common()[-1][0] for posi in positions]
word = ''.join(c for c in word)
logging.info("The unscrambled word: {}".format(word))
