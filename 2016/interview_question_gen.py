"""Simple script to generate one of the topics to revise."""
import numpy as np

def main():
    data_structures = ['linked list',
                       'trees / graph',
                       'Stacks / Queues',
                       'Heaps',
                       'Vector / array list',
                       'hash tables']
    algorithms = ['breadth 1st search',
                  'width 1st search',
                  'binary search',
                  'merge sort',
                  'quick sort']
    concepts = ['bit manipulation',
                'memory (stack vs heap)',
                'recursion',
                'Dynamic programming',
                'big O time / space']
    topics = [data_structures, algorithms, concepts]

    for i in range(3):
        topic_number = np.random.choice(topics)
        question_num = np.random.choice(topic_number)
        print(i, ' - Solve a problem on: ', question_num)

if __name__ == '__main__':
    main()
