"""Day 23 - Advent of code. Opening a turing lock."""


"""
Register and instructions:
Register - small amount of fast storage in the CPU
Instructions - change the value of the register
"""

from utils import read_file

input = read_file('data23')
print(input)

"""
Want to use the get method thing in python
"""

class instructions(object):
    """docstring for instructions."""

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def hlf(self, r, index):
        """Half the register value."""
        return r//2, index+1

    def tpl(self, r, index):
        """Triple the register value."""
        return r*3, index+1

    def inc(self, r, index):
        """Increiment the register by 1"""
        return r+1, index+1

    def jmp(self, offset, index):
        """Jump forwards / backwards the appropriate amount"""
        return index + int(offset)

    def jie(self, r, offset, index):
        """Jump if the register is even."""
        if r % 2 == 0:
            return r, index + int(offset)
        else:
            return r, index + 1

    def jio(self, r, offset, index):
        """Jump if the register is 1."""
        if r == 1:
            return r, index + offset
        else:
            return r, index+1

    def update_register(self, name, new_value):
        """Update the correct name variable."""
        if name == 'a':
            self.a = new_value
        elif name == 'b':
            self.b = new_value
        pass

def proces_offset(offset):
    """Take the string and turn into a +/- int."""
    # print(offset)
    if offset[0] == '-':
        offset = -1*int(offset[1:])
    elif offset[0] == '+':
        offset = 1*int(offset[1:])
    # print("OFFSET", offset)
    return offset


index= 0
I = instructions(a = 1, b = 0)
while index < len(input):
    strip = input[index].replace(',', '')
    split = strip.split(' ')
    instruct = split[0]
    try:
        register = getattr(I, split[1])
    except:
        pass
    if len(split) == 3:
        # print("longer one")
        offset = proces_offset(split[2])

        register, index = getattr(I, split[0])(register, offset, index)
    elif instruct == 'jmp':
        offset = proces_offset(split[1])
        index = getattr(I, split[0])(offset, index)

    else:
        register, index = getattr(I, split[0])(register, index)
    I.update_register(split[1], register)
    # result = method_to_call()
    print("Reg ",split[1], '=', register, "ind ", index, "instruction: ", instruct)
print('Final scores: a = ', I.a, 'b = ', I.b)
