"""Advent of Code 2018 - Day 1: Frequency."""
import time
with open('data/day1.txt', 'r') as f:
    lines = f.readlines()
    lines = [line.strip('\n') for line in lines]
    frequency = 0
    for i, line in enumerate(lines):
        frequency += int(line)
        if i % 100 == 0:
            print('Processed {} lines...'.format(i*100))
    print(frequency)

"""Part 2"""
def scanning(lines):
    log = []
    frequency = 0
    counter = 0
    while True:
        for line in lines:
            counter += 1
            frequency += int(line)
            if frequency in log:
                print("Found repeat!", frequency)
                return False
            else:
                log.append(frequency)
                if counter % 1000 == 0:
                    print('Processed {} lines...'.format(counter*1000))

with open('data/day1.txt', 'r') as f:
    lines = f.readlines()
    lines = [line.strip('\n') for line in lines]
scanning(lines)
