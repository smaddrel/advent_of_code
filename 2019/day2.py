# Day 2 Advent of Code 2019 - gravity Assist."""
"""
Sequences of 4 ints.
0th : 1, 2, 99 - 1 add, 2 multiply, 99 end the program (other number == error)
1st : num1
2nd : num2
3rd : where to store the result
Then step forwards 4 positions for the next bit
"""

with open('data/data2.txt') as f:
    puzzle_input = f.readlines()[0].strip('\n')

input_string = puzzle_input.split(',')
# puzzle_input = [int(input) for input in puzzle_input]
# print(puzzle_input)
# input_string = [1,1,1,4,99,5,6,0,99]

class methods(object):
    """docstring for methods."""

    def __init__(self):
        pass

    def _add(self, x1, x2):
        return int(x1) + int(x2)

    def _mul(self, x1, x2):
        return int(x1) * int(x2)

    def _finish(self, void1, void2):
        # print("Processing complete... exiting.")
        return False

    def _None(self, void1, void2):
        # print("The program has enountered an error... exiting")
        return False

# operations.get_key('key') will return none is key not in dictionary


operations = {'1': 'add',
              '2': 'mul',
              '99': 'finish'}

Methods = methods()

base_input = list(input_string)

# print(input_string)
input_string[1] = 12
input_string[2] = 2
# input_string = [str(num) for num in [1,9,10,3,2,3,11,0,99,30,40,50]]

def interative(input_string):
    val = True
    opt_int = 0
    while val is not False:
        # print(input_string)
        unpacked = input_string[opt_int: opt_int + 4]
        if str(unpacked[0]) not in ['1', '2']:
            # print(unpacked[0])
            val = getattr(Methods, '_' + operations[str(unpacked[0])])(None, None)
        else:
            key, num1, num2, loc = unpacked
            val = getattr(Methods, '_' + operations[str(key)])(input_string[int(num1)], input_string[int(num2)])
            input_string[int(loc)] = str(val)
        opt_int += 4
    return input_string

print('Part 1 value: ', interative(input_string)[0])

# Part 2 --> Find 19690720 at 0
# Waht values go to 1 and 2.
# Values between 0 - 99 (inclusive)

for noun in range(0, 100):
    print('Doing 100x {}'.format(noun))
    for verb in range(0, 100):
        input_string = list(base_input)
        input_string[1] = noun
        input_string[2] = verb
        input_string = interative(input_string)
        if int(input_string[0]) == 19690720:
            print('Noun: ', noun, "Verb: ", verb, "100x noun + verb", 100*noun+verb)
            break
