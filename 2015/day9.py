"""Advent of Code: Day 9, shortest path."""
import utils
import numpy as np
"""
With seven nodes, 7! = 5040 processes...
- cyclical, doing in the same order should give the same result?
"""

def routing(places, places_pairs, min_max='min'):
    """Route the path."""
    all_route_options = []
    for start in places:
        route = {'visited': [], 'length': 0}
        long_route = {'visited': [], 'length': 0}
        route['visited'].append(start)
        long_route['visited'].append(start)
        for i in range(len(places) - 1):
            # short route version
            options = [step for step in places_pairs if step['A'] == route['visited'][-1] \
                        and step['B'] not in route['visited'] or \
                        step['B'] == route['visited'][-1] and step['A'] not in route['visited']]

            if min_max == 'min':
                next_step = min(options, key=lambda opt:int(opt['distance']))
            if min_max == 'max':
                next_step = max(options, key=lambda opt:int(opt['distance']))
            if next_step['A'] == route['visited'][-1]:
                route['visited'].append(next_step['B'])
            else:
                route['visited'].append(next_step['A'])
            route['length'] += int(next_step['distance'])
        all_route_options.append(route)
    return all_route_options


data = utils.read_file('data9')
# data = utils.read_file('data9_test')
places_pairs = []
places = []

for line in data:
    line = line.split(' ')
    places_pairs.append({'A': line[0],
                         'B': line[2],
                         'distance': line[4]})
    if line[0] not in places:
        places.append(line[0])
    if line[2] not in places:
        places.append(line[2])


all_route_options = routing(places, places_pairs, min_max='min')
long_route_options = routing(places, places_pairs, min_max='max')

# What's the optimal route?
best_route = min(all_route_options, key=lambda r: int(r['length']))
Long_route = max(long_route_options, key=lambda r: int(r['length']))
print('The best route :', best_route)
print('The longest route :', Long_route)
