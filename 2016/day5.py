"""Advent of Code Day 5 - 2016 - Game of chess."""

import hashlib
import string

password = ''
"""
CONFIG
"""
# id = 'abc'
id = 'uqwqemis'

""" """

integer = 0
while len(password) < 8:
    id_integer = id + str(integer)
    hash = hashlib.md5(id_integer.encode('utf-8')).hexdigest()
    if hash[:5] == '00000':
        # print('id integer: ', id_integer)
        # print(hash)
        password += str(hash[5])
    integer += 1
    if integer % 10000000 == 0:
        print("Done {} integers...".format(integer))

print("Password (part 1): ", password)

password = [None]*8
integer = 0
while None in password:
    id_integer = id + str(integer)
    hash = hashlib.md5(id_integer.encode('utf-8')).hexdigest()
    if hash[:5] == '00000':
        # print('id integer: ', id_integer)
        # print(hash)
        if hash[5] not in string.ascii_lowercase:
            if int(hash[5]) < 8 and password[int(hash[5])] is None:
                password[int(hash[5])] = str(hash[6])
        else:
            pass
            # print("Invalid")
    integer += 1
    if integer % 10000000 == 0:
        print("Done {} integers...".format(integer))
password = ''.join(s for s in password)
print("Password (part 2): ", password)
