"""Advent of Code 2018 - Day 3: Overlapping fabric request."""
import numpy as np
np.set_printoptions(threshold=np.inf)

def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.readlines()
        lines = [line.strip('\n') for line in lines]
        lines = [line.replace(' ', '') for line in lines]
    return lines

def get_coords(line):
    """Read the coords in and split out the rest."""
    start = line.index('@')
    end = line.index(':')
    coords = [int(x) for x in line[start+1:end].split(',')]
    return coords

def get_dimensions(line):
    """Read the size in and split out the rest."""
    start = line.index(':')
    dimensions = [int(x) for x in line[start+1:].split('x')]
    return dimensions

def update_claim(grid, coords, dimensions):
    """Update the number of claims for space on the grid."""
    # Remember the y goes from the top down, probably doesn't matter...
    for x in range(dimensions[0]):
        for y in range(dimensions[1]):
            grid[coords[0] + x, coords[1] + y] +=  1
    return grid

def check_grid(grid):
    """Check each entry on the grid for overlap."""
    for x in range(dimensions[0]):
        for y in range(dimensions[1]):
            if grid[coords[0] + x, coords[1] + y] > 1:
                return False
    return True


grid = np.zeros((1000,1000), dtype=int)
data = read_file('day3')
for line in data:
    coords = get_coords(line)
    dimensions = get_dimensions(line)
    grid = update_claim(grid, coords, dimensions)
print('Number of spaces with overlap: {}'.format(len(np.where(grid > 1)[0])))
print('Number of spaces claimed: {}'.format(len(np.where(grid > 0)[0])))
print('Percentage of claimed spaces with overlap: {}'.format(len(np.where(grid > 1)[0])/ float(len(np.where(grid > 0)[0]))))

""" Part 2: Which ID has no overlap at all?"""
for line in data:
    coords = get_coords(line)
    dimensions = get_dimensions(line)
    flag = check_grid(grid)
    if flag is True:
        ID = line[:line.index('@')]
        print('Found the seperate section! ID: {}'.format(ID))
        break
