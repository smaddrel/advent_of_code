"""Day 7: Some assemby required."""
import utils

data = utils.read_file('data7')
print(data[0:5])

def parse_line(line):
    """Parse the line and get correct bitwise operator."""
    split_line = line.split(' ')
    print(split_line)
    return split_line

line = parse_line(data[0])
print(~line[1] )
