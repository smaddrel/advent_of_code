""" ----- Advent of code: Day 16 Aunt Sue ------ """
import utils
import pandas as pd
import numpy as np

def process_line(row, number):
    """Read the line and turn into a dict to pass to df."""
    default = {'children': np.nan,
               'cats': np.nan,
               'samoyeds': np.nan,
               'pomeranians': np.nan,
               'akitas': np.nan,
               'vizslas': np.nan,
               'goldfish': np.nan,
               'trees': np.nan,
               'cars': np.nan,
               'perfumes': np.nan,
               'index': np.nan}
    row = row[4:]
    row = row.replace(':', '')
    row = row.replace(',', '')
    row = row.split(' ')
    default['index'] = int(row[0])
    i = 1
    while i < len(row) - 1:
        default[row[i]] = int(row[i + 1])
        i += 2
    return default

file = utils.read_file('data16')
MFCSAM = {'children': 3,
          'cats': 7,
          'samoyeds': 2,
          'pomeranians': 3,
          'akitas': 0,
          'vizslas': 0,
          'goldfish': 5,
          'trees': 3,
          'cars': 2,
          'perfumes': 1,
          }
# Load the data into a pandas df
df = pd.DataFrame(columns = MFCSAM.keys())
for number, row in enumerate(file, 0):
    aunt_dict = process_line(row, number)
    df_temp  = pd.DataFrame([aunt_dict], columns=aunt_dict.keys())
    df = pd.concat([df, df_temp], axis =0).reset_index(drop=True)

# Part 1: Assuming the search terms are exact
df_part1 = df.copy()
for key in MFCSAM.keys():
    df_part1 = df_part1.query('{0} == {1} or ({0} != {0})'.format(key, MFCSAM[key]))
print("Part1: Aunt sue is # {}".format(int(df_part1['index'].values[0])))

# Part 2: assume cats and trees are > x and pomeranians and goldfish are < x
df_part2 = df.copy()
for key in MFCSAM.keys():
    if key  in ['trees', 'cats']:
        df_part2 = df_part2.query('{0} > {1} or ({0} != {0})'.format(key, MFCSAM[key]))
    elif key  in ['pomeranians', 'goldfish']:
        df_part2 = df_part2.query('{0} < {1} or ({0} != {0})'.format(key, MFCSAM[key]))
    else:
        df_part2 = df_part2.query('{0} == {1} or ({0} != {0})'.format(key, MFCSAM[key]))
print("Part2: Aunt sue is # {}".format(int(df_part2['index'].values[0])))
print(df_part2)
