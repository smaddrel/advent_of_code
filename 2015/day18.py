"""Advent of Code day 18 2015."""
from utils import *
import numpy as np
import matplotlib.pyplot as plt
import copy

raw_data = read_file('data18')


def string_to_binary(data):
    """Convert the list of strings to a proper grid format."""
    output = [[0]*102]
    for row in data:
        processed_row = [0 if c == '.' else 1 for c in row]
        processed_row = [0] + processed_row + [0]
        output.append(processed_row)
    output.append([0]*102)
    return output

def update_grid(grid):
    """Update the grid within (1, 101) with the rules."""
    updated_grid = copy.deepcopy(grid)

    for x in range(1, 101):
        for y in range(1, 101):
            count_adjacent = grid[x - 1][y - 1] +\
                             grid[x][y - 1] +\
                             grid[x + 1][y - 1] +\
                             grid[x - 1][y] +\
                             grid[x + 1][y] +\
                             grid[x - 1][y + 1] +\
                             grid[x][y + 1] +\
                             grid[x + 1][y + 1]
            if grid[x][y] == 1:
                if count_adjacent == 2 or count_adjacent == 3:
                    updated_grid[x][y] = 1
                else:
                    updated_grid[x][y] = 0
            if grid[x][y] == 0:
                if count_adjacent == 3:
                    updated_grid[x][y] = 1
                else:
                    updated_grid[x][y] = 0
    return updated_grid

def set_corners_on(data):
    """Simple fix to keep the corners on."""
    data[1][1] = 1
    data[1][100] = 1
    data[100][1] = 1
    data[100][100] = 1
    return data


data = string_to_binary(raw_data)
for i in range(100):
    data = update_grid(data)
lights_on_count = np.sum(np.asarray(data))
print("Total number of lights on:", lights_on_count)

"""Part II: What if the corners are always on?"""
data = string_to_binary(raw_data)
data = set_corners_on(data)
for i in range(100):
    data = update_grid(data)
    data = set_corners_on(data)
lights_on_count = np.sum(np.asarray(data))
print("Total number of lights on (II):", lights_on_count)
