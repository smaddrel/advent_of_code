"""Advent of Code 2019 - Day 4 - Secure Container."""


def valid_password(password, part2=False):
    """Return True if password meets criteria."""
    parsed = [int(c) for c in str(password)]
    if not sorted(parsed) == parsed or len(set(parsed)) == len(parsed):
        return False
    if part2 and not any(parsed.count(x) == 2 for x in set(parsed)):
        return False
    return True


# Part 1: Find the number of valid passwords
input = [int(x) for x in '138307-654504'.split('-')]
num_valid = sum([valid_password(trial) for trial in range(*input)])
print("Part1: Number of valid passwords in range", num_valid)
num_valid = sum([valid_password(trial, part2=True) for trial in range(*input)])
print("Part2: Number of valid passwords in range", num_valid)
# ------------------------
