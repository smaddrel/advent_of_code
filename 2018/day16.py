"""Advent of code 2018: day 16."""


def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.read().split("\n")
    return lines

def addr(register, input):
    A, B, C = input[1:]
    register[C] = register[A] + register[B]
    return register

def addi(register, input):
    A, B, C = input[1:]
    register[C] = register[A] + B
    return register

def mulr(register, input):
    A, B, C = input[1:]
    register[C] = register[A] * register[B]
    return register

def muli(register, input):
    A, B, C = input[1:]
    register[C] = register[A] * B
    return register

def banr(register, input):
    A, B, C = input[1:]
    register[C] = register[A] & register[B]
    return register

def bani(register, input):
    A, B, C = input[1:]
    register[C] = register[A] & B
    return register

def borr(register, input):
    A, B, C = input[1:]
    register[C] = register[A] | register[B]
    return register

def bori(register, input):
    A, B, C = input[1:]
    register[C] = register[A] | B
    return register

def setr(register, input):
    A, B, C = input[1:]
    register[C] = register[A]
    return register

def seti(register, input):
    A, B, C = input[1:]
    register[C] = A
    return register

def gtir(register, input):
    A, B, C = input[1:]
    if A > register[B]:
        register[C] = 1
    else:
        register[C] = 0
    return register

def gtri(register, input):
    A, B, C = input[1:]
    if register[A] > B:
        register[C] = 1
    else:
        register[C] = 0
    return register

def gtrr(register, input):
    A, B, C = input[1:]
    if register[A] > register[B]:
        register[C] = 1
    else:
        register[C] = 0
    return register

def eqir(register, input):
    A, B, C = input[1:]
    if A == register[B]:
        register[C] = 1
    else:
        register[C] = 0
    return register

def eqri(register, input):
    A, B, C = input[1:]
    if register[A] == B:
        register[C] = 1
    else:
        register[C] = 0
    return register

def eqrr(register, input):
    A, B, C = input[1:]
    if register[A] == register[B]:
        register[C] = 1
    else:
        register[C] = 0
    return register


lines = read_file('day16')
list_of_ops = [addr, addi, mulr, muli, banr, bani, borr, bori, setr,
               seti, gtir, gtri, gtrr, eqir, eqri, eqrr]

# Process each line of the file
# test
register = [3, 2, 1, 1]
input = [9, 2, 1, 2]
after = [3, 2, 2, 1]

i = 0
big_count = 0
id_counter = [{'name':str(func), 'nums':[]} for func in list_of_ops]
print(id_counter)
while i < 3227:
    small_count = 0
    register = [int(_) for _ in lines[i][9:19].split(', ')]
    input = [int(_) for _ in lines[i+1].split(' ')]
    after = [int(_) for _ in lines[i+2][9:19].split(', ')]
    i += 4
    # print(register, input, after)
    for id, op in enumerate(list_of_ops):
        output = op(register.copy(), input)
        # print(output)
        if output == after:
            small_count += 1
            for item in id_counter:
                if item['name'] == str(op):
                    if input[0] not in item['nums']:
                        item['nums'].append(input[0])
    if small_count >= 3:
        big_count += 1
        # print("YES")
print("{} cases with 3 or more.".format(big_count))
removed_list = []
sorted_list = sorted(id_counter, key=lambda k: len(k['nums']))
Flag = True
while Flag is True:
    for i, item in enumerate(sorted_list):
        if len(item['nums']) == 1:
            removed = sorted_list.pop(i)
            removed_list.append(item)
            for second in sorted_list:
                if item['nums'][0] in second['nums']:
                    second['nums'].remove(item['nums'][0])
            print(len(removed_list))
            if len(removed_list) == 16:
                Flag = False
print(removed_list)
print(sorted_list)
