"""Advent of Code: Day 11."""
# Power cells
import numpy as np
import matplotlib.pyplot as plt
input = 4842

def power_level(x, y):
    """Calculate the power level per co-ordinate."""
    ID = x + 10
    initial = ID * y
    power = initial + input
    power = power * ID
    scriptable = str(power)
    if len(scriptable) > 2:
        hundreds = int(scriptable[-3])
    else:
        hundreds = 0
    hundreds -= 5
    return hundreds

def sum_square(grid, x, y, size=1):
    sum = np.sum(grid[x:x+size, y:y+size])
    return sum

power_grid = np.zeros((300,300))
for i in range(300):
    for j in range(300):
        power_grid[i, j] = power_level(i, j)

best_coords = [None, None]
best_sum = 0
for i in range(300):
    for j in range(300):
        s = sum_square(power_grid, i, j, size=3)
        if s > best_sum:
            best_sum = s
            best_coords = [i, j]
print('Part 1: ', best_sum, best_coords)
best_coords = [None, None]
best_sum = 0
best_size = 0
counter = 0
for i in range(300):
    for j in range(300):
        bigger = max(i, j)
        for w in range(1,300-bigger):
            # s = sum_square(power_grid, i, j, size=w)
            # s = sum_square(power_grid, i, j, size=w)
            s = np.sum(power_grid[i:i+w, j:j+w])
            if s > best_sum:
                best_sum = s
                best_coords = [i, j]
                best_size = w
            counter += 1
            if counter % 100000 == 0:
                print('Done: {:.4f} # {} {} {}'.format(int(counter), best_sum, best_coords, best_size))

print('Part 2: ', best_sum, best_coords, best_size)
