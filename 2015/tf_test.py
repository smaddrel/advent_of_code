import tensorflow as tf

x = tf.Variable(3, name='x')
y = tf.Variable(4, name='y')

logdir = 'logs/log'

f = x*x*y + y + 2
sess = tf.Session()
init = tf.global_variables_initializer()
file_writer = tf.summary.FileWriter(logdir, tf.get_default_graph())

with tf.Session() as sess:
    init.run()
    result = f.eval()
    # file_writer.add_summary(f)
print(result)
