"""Linked list example."""

class Node(object):
    """docstring for Node."""

    def __init__(self, x, next=None, prev=None):
        super(Node, self).__init__()
        self.val = x
        self.next = next
        self.prev = prev


linked_list = []
xs = [i for i in range(10)]

for i, x in enumerate(xs):
    Ni = Node(x)
    if i > 0:
        Ni.prev = linked_list[i - 1]
        linked_list[i - 1].next = Ni
    linked_list.append(Ni)

for node in linked_list[1:-2]:
    print(node.val, node.prev.val, node.next.val)
