# Day 3:
import matplotlib.pyplot as plt
"""
    Crossing Wires:
        - Follow the path of two wires
        - Find where they cross
        - Get the shortest Manhatten distance to the central port from the X's
"""
with open('data/data3.txt') as f:
    puzzle_input = [wire.strip('\n') for wire in f.readlines()]
# --------
wire1_input = puzzle_input[0].split(',')
wire2_input = puzzle_input[1].split(',')
# wire1_input = 'R8,U5,L5,D3'.split(',')
# wire2_input = 'U7,R6,D4,L4'.split(',')
# wire1_input = 'R75,D30,R83,U83,L12,D49,R71,U7,L72'.split(',')
# wire2_input = 'U62,R66,U55,R34,D71,R55,D58,R83'.split(',')
# -------

class wire(object):
    """docstring for wire."""

    def __init__(self, ):
        self.x_coords = [0]
        self.y_coords = [0]
        self.pairs = []
        self.timer = []

    def U(self, steps):
        self.x_coords += [self.x_coords[-1] for _ in
                          range(self.y_coords[-1] + 1,
                          self.y_coords[-1] + steps + 1)]
        self.y_coords += [y for y in
                          range(self.y_coords[-1] + 1,
                                self.y_coords[-1] + steps + 1)]

    def D(self, steps):
        self.x_coords += [self.x_coords[-1] for _ in
                          range(self.y_coords[-1] - 1,
                                self.y_coords[-1] - steps - 1, -1)]
        self.y_coords += [y for y in
                          range(self.y_coords[-1] - 1,
                                self.y_coords[-1] - steps - 1, -1)]

    def L(self, steps):
        self.y_coords += [self.y_coords[-1] for _ in
                          range(self.x_coords[-1] - 1,
                                self.x_coords[-1] - steps - 1, -1)]
        self.x_coords += [x for x in
                          range(self.x_coords[-1] - 1,
                                self.x_coords[-1] - steps - 1, -1)]

    def R(self, steps):
        self.y_coords += [self.y_coords[-1] for _ in
                          range(self.x_coords[-1] + 1,
                                self.x_coords[-1] + steps + 1)]
        self.x_coords += [x for x in
                          range(self.x_coords[-1] + 1,
                                self.x_coords[-1] + steps + 1)]

    def get_pairs(self, timing=True):
        if timing is False:
            self.pairs = [(x, y) for x, y in zip(self.x_coords,
                                                 self.y_coords)
                          if (x, y) != (0, 0)]
        else:
            for t, (x, y) in enumerate(zip(self.x_coords, self.y_coords)):
                if (x, y) != (0, 0):
                    self.pairs.append((x, y))
                    self.timer.append(t)


def Manhatten(coord_pair, target=(0, 0)):
    """Find the Manhatten distance to target."""
    return abs(coord_pair[0]-target[0]) + abs(coord_pair[1]-target[1])




wire1 = wire()
wire2 = wire()
for step in wire1_input:
    getattr(wire1, step[0])(int(step[1:]))
for step in wire2_input:
    getattr(wire2, step[0])(int(step[1:]))
# Get coords pairs
wire1.get_pairs()
wire2.get_pairs()

crossing_nodes = list(set(wire1.pairs).intersection(wire2.pairs))
# Simple visualisation
plt.plot(wire1.x_coords, wire1.y_coords)
plt.plot(wire2.x_coords, wire2.y_coords)
plt.savefig('results/day3_wires.pdf')
plt.close()

distances = [Manhatten(pair) for pair in crossing_nodes]
print("Part 1: min distance:", min(distances))

# part 2
# Pick the node with the lowest time
# Where T = t_wire1 + t_wire2
wire1_indices = [wire1.pairs.index(node) for node in crossing_nodes]
wire2_indices = [wire2.pairs.index(node) for node in crossing_nodes]

wire1_times = [wire1.timer[t] for t in wire1_indices]
wire2_times = [wire2.timer[t] for t in wire2_indices]

summed_time = [t1 + t2 for t1, t2 in zip(wire1_times, wire2_times)]

print('Part2: min time = {}'.format(min(summed_time)))
