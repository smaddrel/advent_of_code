"""Day 19 Advent of Code : Reindeer Chemisty."""
from utils import read_file
import re

raw_data = read_file('data19')

# ----------------------------

rules = {}
for line in raw_data[:-2]:
    parts = line.split(' => ')
    if parts[0] in rules.keys():
        rules[parts[0]].append(parts[1])
    else:
        rules[parts[0]] = [parts[1]]

# ----------------------------
base_molecule = raw_data[-1]
all_molecules = []
for key in rules:
    for replacement in rules[key]:
        indicies = [m.start() for m in re.finditer(key, base_molecule)]
        for index in indicies:
            test_molecule = "".join((base_molecule[:index],
                                     replacement,
                                     base_molecule[index + len(key):]))
            all_molecules.append(test_molecule)

print('Total unique molecules that can be made: ', len(set(all_molecules)))

# --------------------------
# PART II - # steps to make the molecule from a single e
# --------------------------
"""
1. Start with the full one
2. Then replace one at a time, automatically makes it shorter
3. repeat - then when it stops start again and shuffel the rules
"""

count  = 0
mol = base_molecule
while len(mol) > 1:  # The length means it's found the electron
    start = mol
    for key in rules:
        for replacement in rules[key]:
            count += mol.count(replacement)
            mol = mol.replace(replacement, key)
    if start == mol:
        # No further changes happening
        mol = base_molecule
        count = 0

print('Minimum number of steps found: ', count)
