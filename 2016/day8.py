"""Advent of code - day 8"""
from utils import read_file
import numpy as np
import matplotlib.pyplot as plt

"""
50 x 6 pixels
rect AxB - turns on a rectangle from the top left A wide B tall
rotate row y=A by B - These are shifts not roations.
When the shift reaches the edge it wraps back around to the other side
"""


def rect(x, y, grid):
    # remember, the grid is  indexed y, x
    for y_ in range(y):
        for x_ in range(x):
            grid[y_][x_] = 1.
    return grid

def rotate_column(grid, x=0, rot=0):
    # Shift column x down by rot, loop round to the top
    if rot >= grid[:, x].shape[0]:
        rot -= grid[:, x].shape[0]
    # print([[i -rot] for i in range(grid[:, x].shape[0])])
    grid[:, x] = [grid[:, x][i -rot] for i in range(grid[:, x].shape[0])]
    return grid

def rotate_row(grid, y=0, rot=0):
    # Shift row y across by rot, loop round to the top
    if rot >= grid[y, :].shape[0]:
        rot -= grid[y, :].shape[0]
    # print([[i -rot] for i in range(grid[y, :].shape[0])])
    grid[y, :] = [grid[y, :][i -rot] for i in range(grid[y, :].shape[0])]
    return grid

# Test case:
grid = np.zeros((3, 7))  # Grid is indexed y, x
grid = rect(3, 2, grid)
# print(grid)
grid = rotate_column(grid, x=1, rot=1)
# print(grid)
grid = rotate_row(grid, y=0, rot=4)
# print(grid)
grid = rotate_column(grid, x=1, rot=1)
# print(grid)
count = np.sum(grid)
print("Total lights on: ", int(count))

# Real case
grid = np.zeros((6, 50))  # Grid is indexed y, x
instructions = read_file('data8')

for line in instructions:
    line = line.split(' ')
    if line[0] == 'rect':
        shape_ = [int(i) for i in line[1].split('x')]
        grid = rect(shape_[0], shape_[1], grid)
        # print(line[0], shape_)
    if line[0] == 'rotate':
        if line[1] == 'row':
            y = int(line[2].split('=')[1])
            # print(y)
            grid = rotate_row(grid, y=y, rot=int(line[-1]))
            # print(line[0], line[1], y, int(line[-1]))
        elif line[1] == 'column':
            x = int(line[2].split('=')[1])
            grid = rotate_column(grid, x=x, rot=int(line[-1]))
            # print(line[0], line[1], x, int(line[-1]))
print("Number of illuminated lights: ", int(np.sum(grid)))
plt.imshow(grid)
plt.show()
