"""Advent of code - 2015 - day 21."""
from utils import read_file
import numpy as np
import random as random

"""
RPG Optimisation
- spend gold to buy items
- each round attack and dammage delt / recieved.
-


Weapons:    Cost  Damage  Armor
Dagger        8     4       0
Shortsword   10     5       0
Warhammer    25     6       0
Longsword    40     7       0
Greataxe     74     8       0
"""
Weapons = {'Dagger': {'Cost': 8, 'Damage': 4, 'Armor': 0, 'Type': 'Weapon'},
           'Shortsword': {'Cost': 10, 'Damage': 5, 'Armor': 0, 'Type': 'Weapon'},
           'Warhammer': {'Cost': 25, 'Damage': 6, 'Armor': 0, 'Type': 'Weapon'},
           'Longsword': {'Cost': 40, 'Damage': 7, 'Armor': 0, 'Type': 'Weapon'},
           'Greataxe': {'Cost': 74, 'Damage': 8, 'Armor': 0, 'Type': 'Weapon'},
           }
"""

Armor:      Cost  Damage  Armor
Leather      13     0       1
Chainmail    31     0       2
Splintmail   53     0       3
Bandedmail   75     0       4
Platemail   102     0       5
"""
Armor = {'Leather': {'Cost': 13, 'Damage': 0, 'Armor': 1, 'Type': 'Armor'},
         'Chainmail': {'Cost': 31, 'Damage': 0, 'Armor': 2, 'Type': 'Armor'},
         'Splintmail': {'Cost': 53, 'Damage': 0, 'Armor': 3, 'Type': 'Armor'},
         'Bandedmail': {'Cost': 75, 'Damage': 0, 'Armor': 4, 'Type': 'Armor'},
         'Platemail': {'Cost': 102, 'Damage': 0, 'Armor': 5, 'Type': 'Armor'},
         }
"""
Rings:      Cost  Damage  Armor
Damage +1    25     1       0
Damage +2    50     2       0
Damage +3   100     3       0
Defense +1   20     0       1
Defense +2   40     0       2
Defense +3   80     0       3

"""
Rings = {'A': {'Cost': 25, 'Damage': 1, 'Armor': 0, 'Type': 'Ring'},
         'B': {'Cost': 50, 'Damage': 2, 'Armor': 0, 'Type': 'Ring'},
         'C': {'Cost': 100, 'Damage': 3, 'Armor': 0, 'Type': 'Ring'},
         'D': {'Cost': 20, 'Damage': 0, 'Armor': 1, 'Type': 'Ring'},
         'E': {'Cost': 40, 'Damage': 0, 'Armor': 2, 'Type': 'Ring'},
         'F': {'Cost': 80, 'Damage': 0, 'Armor': 3, 'Type': 'Ring'},
         }

class player(object):
    """docstring for player."""

    def __init__(self, hit_points, attack, deffence):
        self._save_hp = hit_points
        self.hit_points = hit_points
        self.attack = attack
        self.deffence = deffence
        self.has_weapon = False
        self.has_armour = False
        self.number_rings = 0
        self.equipment_cost = 0


    def deffend_from(self, incoming_attack):
        """Takes an attack from the other player and caculates damage taken."""
        damage = self.deffence - incoming_attack
        if damage >= 0:
            damage = 1
        # print("Damage is", damage)
        self.hit_points += damage

    def attack_to(self):
        """Attack the other player, return /calculte the attack stat."""
        return self.attack

    def equipe_item(self, item):
        """Takes the item and equipes it if there is a slot availble."""
        self.deffence = item['Armor']
        self.attack = item['Damage']
        self.equipment_cost = item['Cost']



def combat_turn(player1, player2):
    """Calculates combat where player1 attacks player2."""
    # print("Pre-round:\nAttacking PLayer HP = {}\nDefending player HP= {}".
        # format(player1.hit_points, player2.hit_points))
    status = player2.deffend_from(player1.attack_to())
    # print("Post-round:\nAttacking PLayer HP = {}\nDefending player HP= {}".
        # format(player1.hit_points, player2.hit_points))
    return status

def make_equipment_combinations():
    """Take all equipment and make all combinations."""

    """
    1x weapon
    0 - 1 x armour
    0 - 2 x rings

    """
    def combine_dicts(d1, d2):
        """Combine the vlaues for each dict."""
        return {key:d1[key]+d2[key] for key in d1.keys() if key is not 'Type'}

    weapons_only = [_[1] for _ in Weapons.items()]
    weapons_with_armour = [combine_dicts(W, A[1]) for W in weapons_only for A in Armor.items()]
    weapons_one_ring = ([combine_dicts(W, R[1]) for W in weapons_only for R in Rings.items()])
    # NB these use rings twicw which we cant V
    weapons_two_ring = ([combine_dicts(W, R[1]) for W in weapons_one_ring for R in Rings.items()])
    weapons_one_ring_armor = ([combine_dicts(W, R[1]) for W in weapons_with_armour for R in Rings.items()])
    weapons_two_ring_armor = ([combine_dicts(W, R[1]) for W in weapons_one_ring_armor for R in Rings.items()])

    stack = weapons_only + weapons_with_armour + weapons_one_ring +\
            weapons_one_ring_armor + weapons_two_ring_armor + weapons_two_ring
    return stack

# Part 1 - lowest cost and still win
enemy = player(hit_points=103, attack=9, deffence=2)
me = player(hit_points=100, attack=0, deffence=0)

equipemnt = make_equipment_combinations()
# Sort the equipemnt by cost
equipemnt = sorted(equipemnt, key=lambda k: k['Cost'])
for item in equipemnt:
    me.hit_points = me._save_hp
    me.attack = 0
    me.deffence = 0
    enemy.hit_points = enemy._save_hp
    me.equipe_item(item)
    # combat_turn
    status = 2
    print('\n', item)
    print(me.hit_points, me.attack, me.deffence)
    print(enemy.hit_points, enemy.attack, enemy.deffence)
    while status == 2:
        combat_turn(me, enemy)
        # print(me.hit_points, enemy.hit_points)
        if enemy.hit_points <= 0:
            status = 1
            break
        combat_turn(enemy, me)
        # print(me.hit_points, enemy.hit_points)
        if me.hit_points <= 0:
            if status == 1:
                print("But status == 1...")
            status = 0
            break
    print("Combat outcome = ", status, ', enemy HP = ', enemy.hit_points, ' my hp:', me.hit_points)
    if status == 1:
        break
print(item)

# Part 1I - highest cost and still lose
enemy = player(hit_points=103, attack=9, deffence=2)
me = player(hit_points=100, attack=0, deffence=0)

equipemnt = make_equipment_combinations()
# Sort the equipemnt by cost
equipemnt = sorted(equipemnt, key=lambda k: k['Cost'],reverse=True)
for item in equipemnt:
    me.hit_points = me._save_hp
    me.attack = 0
    me.deffence = 0
    enemy.hit_points = enemy._save_hp
    me.equipe_item(item)
    # combat_turn
    status = 2
    print('\n', item)
    print(me.hit_points, me.attack, me.deffence)
    print(enemy.hit_points, enemy.attack, enemy.deffence)
    while status == 2:
        combat_turn(me, enemy)
        # print(me.hit_points, enemy.hit_points)
        if enemy.hit_points <= 0:
            status = 1
            break
        combat_turn(enemy, me)
        # print(me.hit_points, enemy.hit_points)
        if me.hit_points <= 0:
            if status == 1:
                print("But status == 1...")
            status = 0
            break
    print("Combat outcome = ", status, ', enemy HP = ', enemy.hit_points, ' my hp:', me.hit_points)
    if status == 0:
        break
print(item)
