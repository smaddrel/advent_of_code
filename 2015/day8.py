"""Day 8: Matchsticks."""
import utils
import re
"""
- Count the number of elements in the string literal, i.e. how much memory
- And the number of functional characters in the string
- Want thw total difference
"""

def replace_special_with_placeholder(line):
    """Repalce special characters with X chars - easy counting."""
    short = re.sub(r'\\x[0-9a-f]{2}', "_", line)
    short = re.sub(r'\\"', "_", short)
    short = re.sub(r'\\\\', "_", short)
    string_length = len(short[1:-1])  # For the quotations

    # don't need to touch the hex
    long = re.sub(r'\\', r"\\\\", line)
    long = re.sub(r'"', '\\"', long)
    long = '"' + long + '"'
    longer_length = len(long)

    difference = len(line) - string_length
    exteneded = longer_length - len(line)
    return difference, exteneded


data = utils.read_file('data8')
counter  = 0
counter_ext  = 0
literal, actual = 0, 0

for line in data:
    counter += replace_special_with_placeholder(line)[0]
    counter_ext += replace_special_with_placeholder(line)[1]
print("Part 1:", counter)
print("Part 2:", counter_ext)
# for line in data:
#     literal += len(line)
#     actual  += len(eval(line))
#
# print( literal - actual)
#
# code_len = 0
# short_len = 0
# encoded_len = 0
# for string in data:
# 	string = string.strip()
# 	longer = re.sub(r'\\', r'\\\\', string)
# 	longer = re.sub(r'"', '\\"', longer)
# 	longer = '"' + longer + '"'
# 	short = re.sub(r'\\x[0-9a-fA-Z][0-9a-fA-Z]', '_', string)
# 	short = re.sub(r'\\"', '_', short)
# 	short = re.sub(r'\\\\', '_', short)
# 	short = short[1:len(short)-1]
# 	code_len, short_len, encoded_len = code_len + len(string), short_len + len(short), encoded_len + len(longer)
# 	# print (string, short)
#
# print( 'shortened: ' + str(code_len  - short_len))
# print( 'longer: ' + str(encoded_len - code_len))
