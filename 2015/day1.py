"""Day 1 Advent of Code 2015"""
def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.readlines()
        lines = [line.strip('\n') for line in lines]
    return lines

def check_floor(step):
    """Step checker."""
    up_or_down = {'(': 1, ')': -1}
    return up_or_down[step]

def main():
    """Main function."""
    file = read_file('data1')[0]
    current_floor = 0
    step_into_basement = None
    for i, step in enumerate(file, 1):
        current_floor += check_floor(step)
        if (current_floor == -1) and (step_into_basement is None):
            print(current_floor, step, step_into_basement)
            step_into_basement = i

    print("final floor: {}".format(current_floor))
    print("step into basement: {}".format(step_into_basement))

if __name__ == '__main__':
    main()
