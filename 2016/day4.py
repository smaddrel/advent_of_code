"""Day 4 Advent of code - cypher"""
from utils import read_file
import logging
from collections import Counter
import string


"""
aaaaa-bbb-z-y-x-123[abxyz]
split on the [---] seciton
Then split on the '-'
take the last element as the int
combine the letters and sort by occourance
list the letters in unique occourance
"""
running_sum = 0
input_ = ['aaaaa-bbb-z-y-x-123[abxyz]',
		  'a-b-c-d-e-f-g-h-987[abcde]',
		  'not-a-real-room-404[oarel]',
		  'totally-real-room-200[decoy]']
input_ = read_file('data4')
for line in input_:
	line = line[:-1].split('[')
	letters = line[0].split('-')
	num = int(letters[-1])
	letters_ = letters[:-1]
	letters = ''.join(l for l in letters_)
	letters_ = ''.join(l+' ' for l in letters_)
	counter = Counter(letters)

	sorted_l = sorted(sorted(set(letters)), key= lambda i: counter[i], reverse=True)
	checksum = ''.join(l for l in sorted_l)
	if checksum[:5] == line[-1]:
		running_sum += num
		decrypted = ''
		for l in letters_:
			if l is not ' ':
				id_ = string.ascii_lowercase.index(l)
				l = string.ascii_lowercase[(id_ + num) % 26]
			decrypted += l
		if decrypted == 'northpole object storage ':
			print('NP Storage @: ', num)

print("Total running sum:", running_sum)

"""Part 2 is a shift cypher - rotate each letter through X steps in the alhabet."""
