"""Day 2 - Advent of Code 2016 - Bathroom Security."""
from utils import read_file
import logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s --- %(message)s')


instructions = read_file('data2')
# instructions = ['ULL', 'RRDDD', 'LURDL', 'UUUUD']
class number_pad(object):
    """docstring for number_pad."""

    def __init__(self, part):
        self.part = part

    def L(self, input):
        if self.part == 1:
            check = [1, 4, 7]
            if input not in check:
                return input - 1
            return input
        else:
            if input == 9:
                return 8
            elif input == 4:
                return 3
            elif input == 8:
                return 7
            elif input == 12:
                return 11
            elif input == 3:
                return 2
            elif input == 7:
                return 6
            elif input == 11:
                return 10
            elif input == 6:
                return 5
            else:
                return input


    def R(self, input):
        if self.part == 1:
            check = [3, 6, 9]
            if input not in check:
                return input + 1
            return input
        else:
            if input == 5:
                return 6
            elif input == 2:
                return 3
            elif input == 6:
                return 7
            elif input == 10:
                return 11
            elif input == 3:
                return 4
            elif input == 7:
                return 8
            elif input == 11:
                return 12
            elif input == 8:
                return 9
            else:
                return input


    def U(self, input):
        if self.part == 1:
            check = [1, 2, 3]
            if input not in check:
                return input - 3
            return input
        else:
            if input == 13:
                return 11
            elif input == 10:
                return 6
            elif input == 11:
                return 7
            elif input == 12:
                return 8
            elif input == 6:
                return 2
            elif input == 7:
                return 3
            elif input == 8:
                return 4
            elif input == 3:
                return 1
            else:
                return input


    def D(self, input):
        if self.part == 1:
            check = [7, 8, 9]
            if input not in check:
                return input + 3
            return input
        else:
            if input == 1:
                return 3
            elif input == 2:
                return 6
            elif input == 3:
                return 7
            elif input == 4:
                return 8
            elif input == 6:
                return 10
            elif input == 7:
                return 11
            elif input == 8:
                return 12
            elif input == 11:
                return 13
            else:
                return input


# ----- PART 1 -----
val = 5
code = []
NumPad = number_pad(part=1)
for move in instructions:
    for c in move:
        val = getattr(NumPad, c)(val)
    code.append(val)
code_string = ''.join(str(i) for i in code)
logging.info("(part 1) The bathroom code is: {}".format(code_string))

# ----- PART 2 -----
val = 5
code = []
NumPad = number_pad(part=2)
for move in instructions:
    for c in move:
        val = getattr(NumPad, c)(val)
    code.append(val)
updated_code = []
for c in code:
    if c == 10:
        updated_code.append('A')
    elif c == 11:
        updated_code.append('B')
    elif c == 12:
        updated_code.append('C')
    elif c == 13:
        updated_code.append('D')
    else:
        updated_code.append(c)
code_string = ''.join(str(i) for i in updated_code)
logging.info("(part 2) The bathroom code is: {}".format(code_string))
