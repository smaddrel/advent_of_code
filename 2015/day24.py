"""Advent of code day 24 - 2015."""
from utils import read_file
from itertools import combinations
from functools import reduce

"""
- 3 groups - must weigh the same
- G1 - fewest number of packages possible
- If G1 has multiple combinations then min product of QE
"""

def product(numbers):
    """Calculate the product of the input"""
    value = 1
    for num in numbers:
        value *= num
    return value

input = read_file('data24')
input = [int(x) for x in input]
print(input)
def get_qe(input, number):
    equal_weight = sum(input)/number
    combs = []
    for i in range(len(input[:9])):
        combs += combinations(input, i)
    reduced_list = [list(c) for c in combs if sum(c) == equal_weight]
    reduced_list = sorted(reduced_list, key=len, reverse=False)
    smallest = [sublist for sublist in reduced_list if len(sublist) == len(reduced_list[0])]
    QE_smallest = sorted([product(x) for x in smallest])
    print("QE for {number} packages: {QE_smallest}".format(number=number, QE_smallest=QE_smallest[0]))
    print("Done")

get_qe(input, 3)
get_qe(input, 4)
