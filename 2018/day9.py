"""Advent of Code: day 9, Marble Game."""
from itertools import cycle

num_players = 10
last_value = 1618

players = [{'ID': i, 'score': 0} for i in range(num_players)]
pool = cycle(players)
circle = []

index = 0
for i in range(50):
    player = next(pool)
    if (i % 23 == 0) and (i > 0):
        player['score'] += i
        player['score'] += circle[-7]
        circle.pop(-7)
        if index > 6:
            index = index - 7
        else:
            index = len(circle) - (7 - index)
    else:
        if index == len(circle):
            index = 0
        else:
            index += 1
        circle.insert(index, i)
        print(circle)

sorted_scores = sorted(players, key=lambda k: k['score'], reverse=True)
print(sorted_scores[:10])
