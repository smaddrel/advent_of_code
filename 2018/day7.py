"""Advent of code day 7: Flow, order of processes"""

def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.read().split("\n")
    return lines[:-1]


"""Need to find a way to build a graph properly. Then control the flow?"""
