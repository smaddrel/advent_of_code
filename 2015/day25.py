"""Advent of code day 25 - 2015."""
import numpy as np
"""
To continue, please consult the code grid in the manual.  Enter the code at row 2981, column 3075.

Manual:
   |    1         2         3         4         5         6
---+---------+---------+---------+---------+---------+---------+
 1 | 20151125  18749137  17289845  30943339  10071777  33511524
 2 | 31916031  21629792  16929656   7726640  15514188   4041754
 3 | 16080970   8057251   1601130   7981243  11661866  16474243
 4 | 24592653  32451966  21345942   9380097  10600672  31527494
 5 |    77061  17552253  28094349   6899651   9250759  31663883
 6 | 33071741   6796745  25397450  24659492   1534922  27995004
"""

def get_next_code(input):
    """times by 252533, then divide by 33554393, th remainder is the code."""
    code = input * 252533
    remainder = code % 33554393
    return remainder
def get_nth_coord(row, col):
    """Get the n the coresponds to the row col option."""
    return (row + col - 2) * (row + col - 1) // 2 + col

code = 20151125
for i in range(1, get_nth_coord(2981, 3075)):
    code = get_next_code(code)
print(i, code)

"""Translate co-ordinate into total number of steps."""
"""
1 - (1, 1)
2 - (1, 2)
3 - (2, 1)
4 - (1, 3)
5 - (2, 2)
6 - (3, 1)
"""
