"""Advent of COde day 15: Ingredients."""
import utils

def process_to_dict(data):
    """Split on :, then , then space."""
    ingredients = {}
    for line in data:
        line = line.split(':')
        tmp = line[1].split(',')
        properties = {part.strip().split(' ')[0]:
                      int(part.strip().split(' ')[1]) for part in tmp}
        ingredients[line[0]] = properties
    return ingredients


raw_data = utils.read_file('data15')
raw_data = ['Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8', 'Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3']
ingredients = process_to_dict(raw_data)  # dict of dicts of ingredients
scoring_properties = ['capacity', 'durability', 'flavor', 'texture']

# Add the scale factor for each ingredient
for ingred in ingredients:
    factor = sum([ingredients[ingred][key] for key in scoring_properties])
    ingredients[ingred]['factor'] = factor


tablespoons = [44, 56]
scores = 1
for prop in scoring_properties:
    print(prop)
    prop_score = 0
    for n_ing, ingred in zip(tablespoons, ingredients):
        prop_score += n_ing * ingredients[ingred][prop]
    if prop_score < 0:
        prop_score = 0
    print(scores, prop_score, scores * prop_score )
    scores *= prop_score
