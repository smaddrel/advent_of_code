"""Advent of Code Day 12: Growing plants."""

def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.read().split("\n")
    return lines[:-1]

def create_rules_dict(line):
    line = line.split(' => ')
    return {'rule': line[0], 'replace': line[1]}

def update_pots(rules, current_state, center_id):

    Flag = False
    current_state = '....' + current_state + '....'
    new_state = list(current_state)
    for i in range(0, len(current_state)-2):
        if current_state[i+2] == '.' and Flag is False:
            continue
        else:
            Flag = i
        for rule in rules:
            if current_state[i-2:i+3] == rule['rule']:
                new_state[i] = rule['replace']
    while new_state[-1] == '.':  # remove trailing zeroes
        new_state = new_state[:-1]
    new_state = new_state[Flag:]

    new_state = ''.join(new_state)
    center_id += 4
    center_id -= Flag
    return new_state, center_id

def sum_pot_ids(state, center_id):
    # center = int(len(state)/2)
    value = 0
    for i, pot in enumerate(state, -center_id):
        if pot == '#':
            value += i
    return value



if __name__ == '__main__':
    lines = read_file('day12')
    lines[0].replace(':', " ")
    state = lines[0].split()[2]

    rules_dicts = [create_rules_dict(line) for line in lines[2:]]
    center_id = 0
    for epoch in range(20):
        state, center_id = update_pots(rules_dicts, state, center_id)
    value = sum_pot_ids(state, center_id)

    print('After 20 epochs the value is :', value)
    lines = read_file('day12')
    lines[0].replace(':', " ")
    state = lines[0].split()[2]

    rules_dicts = [create_rules_dict(line) for line in lines[2:]]
    center_id = 0
    for epoch in range(50000000000):
        if epoch % 100000 == 0:
            print('{}/{}'.format(epoch, 50000000000))
        state, center_id = update_pots(rules_dicts, state, center_id)
        value = sum_pot_ids(state, center_id)
    print('After 50000000000 epochs the value is :', value)
