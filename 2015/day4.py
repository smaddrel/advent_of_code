"""Advent of Code 2015 - day 4 - Advent Coin."""
import hashlib
secret = 'yzbqklnj'


i = 0
status5 = True
status6 = True
while (status5 is True) or (status6 is True):
    md5 = hashlib.md5()
    md5.update((secret+str(i)).encode('utf-8'))
    if md5.hexdigest()[:5] == '0'*5 and status5 is True:
        print('five', i)
        status5 = False
    if md5.hexdigest()[:6] == '0'*6 and status6 is True:
        print('six', i)
        status6 = False
    i += 1
