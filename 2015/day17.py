"""Day 17 Advent of Code: permutations."""
import numpy as np
from itertools import combinations
containers = [43, 3, 4, 10, 21, 44, 4, 6, 47, 41, 34, 17,
              17, 44, 36, 31, 46, 9, 27, 38]
target = 150
print("average:", np.mean(containers))
print('number:', len(containers))
"""
Question: How many ways can the containers be used to make 150?
- No duplicates once out of list.
- Brute force isn't going to cut this
- Average of 6 containers
- order doesn't matter, only combinations
- this needs some serious recursion
- simple :
    - Pick a number and remove from list
    - repeat until
"""
"""
Part I : Getting the total number of combinations to fill 150
"""
counter = 0
for s in range(len(containers)):
    for subset in combinations(containers, s):
        if sum(subset) == 150:
            counter += 1
print('Part I, how many can fill 150l? Answer:', counter)
"""
Part II : Getting the min number of combinations to fill 150 + how many options
"""
counter = 0
minimum = 1e9
for s in range(len(containers)):
    for subset in combinations(containers, s):
        if sum(subset) == 150:
            if minimum == len(subset):
                counter += 1
            if len(subset) < minimum:
                minimum = len(subset)
                counter = 1
print('Part II, how small can fill 150l? Answer:', counter, 'length', minimum)
