"""Day 7 - Snoopers Charter..."""
from utils import read_file
import string

real_input = read_file('data7')
test_input = ['abba[mnop]qrst', 'abcd[bddb]xyyx',
              'aaaa[qwer]tyui', 'ioxxoj[asdfgh]zxcvbn']

"""
1. Find a pair of letters, then check the surounding letters
2. The quad has to be outside the square brackets
3. If there is one in the square brackets the whole thing is not valid
4. String format  XXXXX[XXXX]XXXX -> split by []
"""


def truth_match(sub_string):
    i = 0
    while i < len(sub_string) - 2:
        # print(IP_ad[i], IP_ad[i+1])
        if sub_string[i] == sub_string[i+1]:
            # print("Found a pair of :", sub_string[i])
            if sub_string[i-1] == sub_string[i+2] and sub_string[i] != sub_string[i+2]:
                # print("Found a pair of :", sub_string[i+2])
                return True
        i += 1
    return False


def result_flag(results):
    flag = False
    print(results)
    for i, result in enumerate(results):
        # print(i, i%2, result)
        if i % 2 == 0 and result is True:
            # print(i % 2)
            flag = True
        if i % 2 == 1 and result is True:
            # print(i % 2)
            print("nah")
            return False
    return flag
    # return (result[0] or result[2]) and not result[1]


def process(ip):
    IP_ad = ip.replace('[', ']').split(']')  # Love this chaining!
    return result_flag([truth_match(sub) for sub in IP_ad])


input_data = test_input
input_data = real_input[:15]


number_vlaid = sum([process(ip) for ip in input_data])
print("Part1: Number of valid IP addresses {}/{}".
      format(number_vlaid, len(input_data)))
