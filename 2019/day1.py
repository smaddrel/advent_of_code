# 2019 Advent of Code - Day 1
import math
with open('data/data1.txt') as f:
    puzzle_input = f.readlines()
puzzle_input = [int(input.strip('\n')) for input in puzzle_input]
# div by three, round down, subtract 2
sum_modules = sum([math.floor(x / 3) - 2 for x in puzzle_input])

print(sum_modules)

# Part 2
def calc(input):
    # print(input)
    val = math.floor(input / 3) - 2
    # print(val)
    if val > 0:
        return input+calc(val)
    else:
        return input

sum_modules = sum([calc(num) - num for num in puzzle_input])
print(sum_modules)
