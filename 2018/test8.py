def buildtree(nodes):
    children_n, meta_n = next(nodes), next(nodes)
    return {'children': [buildtree(nodes) for _ in range(children_n)],
            'meta': [next(nodes) for _ in range(meta_n)]}

def part1(node):
    return sum(node['meta']) + sum(part1(c) for c in node['children'])

def part2(node):
    if not node['children']: return sum(node['meta'])
    return sum(part2(node['children'][m-1])
               for m in node['meta'] if 0 < m <= len(node['children']))

tree = buildtree(map(int, open('data/day8.txt').read().strip().split()))
zwprint(part1(tree), part2(tree))
