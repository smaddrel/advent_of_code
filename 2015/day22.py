"""Day 22 Advent of Code - RPG 20XX - Wizards this time."""

# The various types of spells that can be cast
magic_missile = {'cost': 53, 'damage': 4, 'healing': 0, 'defence': 0,
                 'mana': 0, 'type': 'attack', 'duration': None}
drain = {'cost': 73, 'damage': 2, 'healing': 2, 'defence': 0,
         'mana': 0, 'type': 'attack', 'duration': None}
# effects 
shield = {'cost': 113, 'damage': 0, 'healing': 0, 'defence': 7,
          'mana': 0, 'type': 'effect', 'duration': 6}
poison = {'cost': 173, 'damage': 3, 'healing': 0, 'defence': 0,
          'mana': 0, 'type': 'effect', 'duration': 3}
recharge = {'cost': 229, 'damage': 0, 'healing': 0, 'defence': 0,
            'mana': 101, 'type': 'effect', 'duration': 5}

class player(object):
    """docstring for player."""

    def __init__(self, name, hit_points, armour, attack, mana):
        self.name = name
        self.hit_points = hit_points
        self.armour = armour
        self.attack = attack
        self.mana = mana
        self.effect_counter = 0
        self.effect = None

    def get_attack(self):
        """Get the attack value and modualte for effects."""
        if self.effect_counter > 0 and self.effect['type'] == 'attack':
            modifier = self.effect['attack']
            return self.attack + modifier
        return self.attack

    def get_deffence(self):
        """Get the deffence value and modualte for effects."""
        if self.effect_counter > 0 and self.effect['type'] == 'defence':
            modifier = self.effect['defence']
            return self.defence + modifier
        return self.defence

    def get_mana(self):
        """Get the mana value and modualte for effects."""
        if self.effect_counter > 0 and self.effect['type'] == 'mana':
            modifier = self.effect['mana']
            return self.mana + modifier
        return self.mana

# Set out all the combinations
"""
1. The order of everything matters...
2. How would we select all combinations, in all time purmutations
3.

"""
