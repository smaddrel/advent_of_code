"""Day 10: Look - Say Game."""
import datetime
input = '1113122113'

print(input)
for i in range(60):
    t0 = datetime.datetime.now()
    tracker = {'number': None, 'repeats': 0}
    next_step = ''
    for num in input:
        if tracker['number'] != num:
            if tracker['number'] is not None:
                next_step +=  str(tracker['repeats']) + str(tracker['number'])
            tracker['number'] = num
            tracker['repeats'] = 1
        else:
            tracker['repeats'] += 1
    next_step +=  str(tracker['repeats']) + str(tracker['number'])
    input = next_step
    t1 = datetime.datetime.now()
    print('Generation: {}, length = {}, time: {}'.format(i, len(input), t1 - t0))
