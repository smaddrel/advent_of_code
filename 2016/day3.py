"""Advent of code - Day 3 - 2016."""
from utils import read_file
import logging
logging.basicConfig(level=logging.DEBUG)


input = read_file('data3')
# logging.info(input)


def parse_line(line):
    """Parse the line and return the three numbers."""
    line = line.strip(' ')
    line = line.split(' ')
    line = [int(l) for l in line if l is not '']
    return line


def is_triangle(input):
    """Are all permutations valid? - check."""
    if (input[0] + input[1] > input[2]) and (input[1] + input[2] > input[0])\
            and (input[2] + input[0] > input[1]):
        return 1
    else:
        return 0


def parse_line_2(input):
    """Read three lines, return three triangles."""
    read_lines = []
    counter = 0
    for line in input:
        read_lines.append(parse_line(line))
    for j in range(3):
        tri = [read_lines[i][j] for i in range(3)]
        counter += is_triangle(tri)
    return counter


# ----- Part 1 -----
tringle_counter = 0
for inp in input[:-1]:
    line = parse_line(inp)
    tringle_counter += is_triangle(line)

logging.info("-----Part1: There are {} triangles.".format(tringle_counter))

# ----- Part 2 -----
tringle_counter = 0
for row in range(len(input)//3):
    tringle_counter += parse_line_2(input[row*3:(row+1)*3])


logging.info("-----Part2: There are {} triangles.".format(tringle_counter))
# --- end ---
