"""Day 20 of 2015 Advent of code : infinte houses."""
from datetime import datetime
from functools import reduce
import numpy as np


"""
This is really good, learn why this works, very good to have in a back pocket
||||||
VVVVVV
"""

def factors(n):
    return set(reduce(list.__add__,
                ([i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0)))



# Part 1
target = 33100000
house_number = 776160
max = 0
house_sum = 0
# for i in range(1, target):
start = datetime.now()
while house_sum < target:
    house_number += 1
    Facs = factors(house_number)
    house_sum = sum([num*10 for num in Facs])
    if house_sum > max:
        max = house_sum
    if house_number % 5000 == 0:
        end = datetime.now()
        print(house_number, house_number/target, end - start, house_sum, '\t\t', max/target)
        start = datetime.now()
print("Found the solution!")
print(house_number, house_number/target, house_sum, '\t\t', max/target, '\n\n\n')

# Part 2
time_sheet = {}

def check_time_sheet(factor):
    """Check the timesheet dict for use."""
    if factor not in time_sheet.keys():
        time_sheet[factor] = 1
        return 11*factor
    elif time_sheet[factor] > 50:
        return 0
    else:
        time_sheet[factor] += 1
        return factor * 11

target = 33100000
house_number = 0
max = 0
house_sum = 0
# for i in range(1, target):
start = datetime.now()
while house_sum < target:
    house_number += 1
    Facs = factors(house_number)
    house_sum = sum([check_time_sheet(num) for num in Facs])
    if house_sum > max:
        max = house_sum
    if house_number % 5000 == 0:
        end = datetime.now()
        print(house_number, house_number/target, end - start, house_sum, '\t\t', max/target)
        start = datetime.now()
print("Found the solution!")
print(house_number, house_number/target, house_sum, '\t\t', max/target, '\n\n\n')
