"""Advent of Code: Day 6 Manhatten Distance."""
import matplotlib.pyplot as plt
import numpy as np


def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.read().split("\n")
    return lines[:-1]


def distance_map(X, Y):
    """Create a map of manhatten distance."""
    grid = np.zeros((360, 360))
    for x in range(360):
        for y in range(360):
            manhatten_d = abs(X - x) + abs(Y - y)
            grid[x, y] = manhatten_d
    return grid

def check_grid(grids, x, y):
    """Check the index of the lowest distance for x, y."""
    lowest = 1e6
    index = None
    for i, grid in enumerate(grids):
        distance = grid[x, y]
        # TODO: Remove contested points
        if distance == lowest:
            index = 0
        if distance < lowest:
            lowest = distance
            index = i
    return index


coords = read_file('day6')
X = [int(x.split(',')[0]) for x in coords]
Y = [int(y.split(',')[1]) for y in coords]

grids = []  # These will be the same order as the coords
for x, y, in zip(X, Y):
    grids.append(distance_map(x, y))

Xs = []
Ys = []
inds = []
inds_to_remove = []
for x in range(min(X), max(X)):
    for y in range(min(Y), max(Y)):
        index = check_grid(grids, x, y)
        Xs.append(x)
        Ys.append(y)
        inds.append(index)
        if (x == min(X)) or (x == max(X)) or (y == min(Y)) or (y == max(Y)):
            inds_to_remove.append(index)

inds_to_remove = set(inds_to_remove)
""" Remove the regions of the given indicies."""
final_x = []
final_y = []
final_ind = []
for x, y, ind in zip(Xs, Ys, inds):
    if ind not in inds_to_remove:
        final_x.append(x)
        final_y.append(y)
        final_ind.append(ind)
from collections import Counter
counted = Counter(final_ind)
print(counted)
index_of_largest = max(counted, key=counted.get)
print("The biggest (bound) area at: ", X[index_of_largest], Y[index_of_largest])
print("The biggest (bound) area size: ", counted[index_of_largest])

plt.scatter(final_x, final_y, c=final_ind)
plt.scatter(X, Y,)
plt.scatter(X[index_of_largest], Y[index_of_largest], c='r')
plt.show()




"""
(1) Get a distance map for each coord
(2) Assign each point in the grid to the coord with lowest distance at x, y
    (50 co-ordinates)
(3) Count the coord with the most values
(4) How do we deal with the ones at the edge?
    - How to remove ones on the edges?
"""
