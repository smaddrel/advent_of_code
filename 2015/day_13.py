"""Day 13: Knights of the Dinner Table."""
import utils
import itertools
happiesness_data = utils.read_file('data13')

"""
How to parse this data?
- start with a dict, keys for each guest, then list of tuples containing
partner and happiesness delta
"""
guests = ["Alice", "Bob", "Carol", "David", "Eric", "Frank", "George", "Mallory"]


def process_line(line):
    """Read each line, split and return appropriate tuple."""
    line = line.strip('.')
    line = line.split()
    person = line[0]
    if line[2] == 'lose':
        factor = -1
    else:
        factor = 1
    guest_tuple = (int(line[3])*factor, line[-1])
    return person, guest_tuple


def get_haps(variation, prefs):
    """Take the order and sore the seating arrangement."""
    score = 0
    for i, guest in enumerate(variation, 0):
        if i > 0:
            left = i - 1
        else:
            left = len(variation) - 1
        if i < len(variation) - 1:
            right = i + 1
        else:
            right = 0
        guest_left = variation[left]
        guest_right = variation[right]
        score += [hap[0] for hap in prefs[guest] if hap[1] == guest_right][0]
        score += [hap[0] for hap in prefs[guest] if hap[1] == guest_left][0]
    return score

def get_rankings(guests, preferenes):
    """Get the rankigns"""
    rankings = []
    for variation in itertools.permutations(guests):
        score = get_haps(variation, preferenes)
        rankings.append((score, variation))
    return rankings
preferenes = {guest: [] for guest in guests}

for line in happiesness_data:
    person, tup = process_line(line)
    preferenes[person].append(tup)

rankings = get_rankings(guests, preferenes)
sorted_rank = sorted(rankings, key=lambda t: t[0], reverse=True)
print('Optimal seating: ', sorted_rank[0])
# Part 2: Adding me as well!
for guest in guests:
    preferenes[guest].append((0, 'me'))
preferenes['me'] = []
for guest in guests:
    preferenes['me'].append((0, guest))
guests += ['me']
print(guests)
rankings = get_rankings(guests, preferenes)
sorted_rank = sorted(rankings, key=lambda t: t[0], reverse=True)
print('Optimal seating (with me): ', sorted_rank[0])
