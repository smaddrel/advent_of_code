"""Advent of Code: Day 17 - water Flow."""
import matplotlib.pyplot as plt
import numpy as np

def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.read().split("\n")
    return lines[:-1]


data = read_file('day17')
X = []
Y = []
for line in data:
    line = line.split(', ')
    if line[0][0] == 'x':
        x = int(line[0][2:])
        ys = line[1][2:].split('..')
        y_low = int(ys[0])
        y_high = int(ys[1])
        print(x, y_low, y_high)
        for y in range(y_low, y_high+1):
            Y.append(y)
            X.append(x)
    else:
        y = int(line[0][2:])
        xs = line[1][2:].split('..')
        x_low = int(xs[0])
        x_high = int(xs[1])
        print(y, x_low, x_high)
        for x in range(x_low, x_high+1):
            Y.append(y)
            X.append(x)

x_dims = (min(X), max(X))
y_dims = (min(Y), max(Y))
X = [x - x_dims[0] for x in X]
Y = [y - y_dims[0] for y in Y]
print(x_dims, y_dims)

grid = np.zeros((x_dims[0], y_dims[1]))
for x, y in zip(X, Y):
    grid[x,y] = 1

plt.imshow(grid)
plt.show()
