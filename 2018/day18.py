"""Advent of code day 18."""
import numpy as np


def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.read().split("\n")
    return lines[:-1]

def count_surroundings(maps, x, y):
    trees = 0
    lumber_yards = 0
    empty = 0
    for i in range(x-1,x+2):
        for j in range(y-1,y+2):
            if (i != x) and(j != y):
                if maps[i][j] == '|':
                    trees += 1
                if maps[i][j] == '.':
                    empty += 1
                if maps[i][j] == '#':
                    lumber_yards += 1
    return [trees, lumber_yards, empty]

def update(current, surroundings):
    if (current == '|') and (surroundings[1] >= 3):
        new = '#'
    elif (current == '.') and (surroundings[0] >= 3):
        new = '|'
    elif (current == '#') and (surroundings[2] <= 3):
        new = '.'
    else:
        new = current
    return new

map_ = read_file('day18')
maps = []
for row in map_:
    maps.append([0]+[c for c in row]+[0])

temp_maps = list(maps)
for i in range(1, 51):
    for j in range(1, 51):
        surroudings = count_surroundings(maps, i, j)
        new = update(maps[i][j], surroudings)
        temp_maps[i][j] = new
print(temp_maps)
