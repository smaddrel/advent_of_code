"""Advent of Code: Day 4. Guard Duty."""
# Part 1: Which guard spends the most time asleep?
# And which minuite are they asleep the most at?

def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.readlines()
        lines = [line.strip('\n') for line in lines]
    return lines

def add_guard(id):
    guard_dict = {'ID': id, 'sleeping':[]}
    return guard_dict

def sort_entries(lines):
    """Sort the entries in some recursive manner."""
    # Dead easy, python is so clever
    lines.sort(key=lambda x: x[1:17])
    # Sorts in place so we return None to remind us
    return None

def find_index(guards, ID):
    for i, d in enumerate(guards):
        if d['ID'] == ID:
            return i
    return -1


def process(guard_log, line, line_gen):
    # if line[2] == 'Guard':
    ID = int(line[3][1:])
    if not any(ID in d for d in guard_log):
        guard_log.append(add_guard(ID))
    line = next(line_gen).split()
    if line[2] == 'falls':
        t0 = int(line[1][3:5])
        line = next(line_gen).split()
        if line[2] == 'wakes':
            t1 = int(line[1][3:5])
            index = find_index(guard_log, ID)
            guard_log[index]['sleeping'].extend([_ for _ in range(t0, t1)])
        elif line[2] == 'Guard':
            print("Didn't wake up?")
            # Didn't wake up? Does this ever happen? No
            pass
    elif line[2] == 'Guard':
        guard_log = process(guard_log, line, line_gen)

    return guard_log


lines = read_file('day4')
sort_entries(lines)
guard_log = []
line_gen = iter(lines)
for line in line_gen:
    print(line)
    line = line.split()
    if line[2] == 'Guard':
        guard_log = process(guard_log, line, line_gen)
# Now who sleeps the most?
sleepy = sorted(guard_log, key=lambda x: len(x['sleeping']), reverse=True)[0]
print("The sleepiest guard: {}".format(sleepy['ID']))
print("Spent a total of {} minutes asleep".format(len(sleepy['sleeping'])))
print("Most asleep at {} ".format(max(set(sleepy['sleeping']), key=sleepy['sleeping'].count)))
print("Puzzle answer: {}".format(sleepy['ID']*max(set(sleepy['sleeping']), key=sleepy['sleeping'].count)))
from collections import Counter
print(Counter(sleepy['sleeping']))
