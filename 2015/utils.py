"""Utility functions for Advent of Code."""
def properties(func):
    """Decorator that prints properties of the file that's read in."""
    return func

@properties
def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.readlines()
        lines = [line.strip('\n') for line in lines]
    return lines
