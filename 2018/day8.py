"""Advent of Code Day 8: Memory Maneuver."""


"""What is th sum of all the meta-data entries?
rules:
(1) head (x, y) number of nodes and number of spare entries
(2) Nodes are of equal length
"""

def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.read().split("\n")
    return lines[:-1]

def split(a, n):
    k, m = divmod(len(a), n)
    return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))

def split_header(node):
    """Take the first two numbers and return split out the list."""
    number_of_nodes = node[0]
    number_of_meta = node[1]
    next_node = node[2:-number_of_meta]
    # split the next into all the nodes.
    sum_of_meta = sum(node[-number_of_meta:])
    next_node = list(split(next_node, number_of_nodes))
    return next_node, sum_of_meta

data = read_file('day8')
data = [int(d) for d in data[0].split()]
# print(data)
sum_of_meta = 0
next_node, meta_sum = split_header(data)
sum_of_meta += meta_sum
print(sum_of_meta)
while True:
    if len(next_node) < 1:
        break
    for node in next_node:
        print(node)
        next_node, meta_sum = split_header(node)
        sum_of_meta += meta_sum
        print(sum_of_meta)
        remove = []
        for i, node in enumerate(next_node):
            if len(node) < 3:
                remove.append(i)
        for i in sorted(remove, reverse=True):
            next_node.pop(i)

    print(sum_of_meta)
