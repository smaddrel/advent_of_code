"""Day 5: Filtering down the polymers in the fabric, by matching letters."""

def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.read().split("\n")[0]
    return lines


def remove_pairs(polymer):
    print("scanning...")
    to_remove = []
    for i in range(1, len(polymer)):
        first = polymer[i- 1]
        second = polymer[i]
        if first.lower() == second.lower():
            # print('got a pair', first, second)
            if (first.islower() and second.isupper()) or (first.isupper() and second.islower()):
                to_remove.extend([i-1, i])
                # polymer.pop(i)
                # polymer.pop(i-1)
                # i -= 1
                # print('removing', first, second)
    # print('to remove', to_remove)

    if len(to_remove) > 0:
        polymer = [i for j, i in enumerate(polymer) if j not in to_remove]
        # print("stripped")
        #
        # for index in sorted(to_remove, reverse=True):
        #     print('removing', index, polymer[index])
        #     del polymer[index]
        polymer = remove_pairs(polymer)
    return polymer



polymer = read_file('day5')
print(len(polymer), 'entries to start')
polymer = list(polymer)
polymer = remove_pairs(polymer)

print('Length of final polymer is :', len(polymer))
#
