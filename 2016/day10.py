"""Advent of code 2016 day 10."""
from utils import read_file

data = read_file('data10')
counter = 0
for line in data:
    if line[:5] == 'value':
        print(line)
        counter += 1
print(counter)

"""
The point is we dont know where the start vlaues / end values are
- Can we track the vlaues 61 and 17 through the system?
- Only 21 individual vlaues to track
-
"""
