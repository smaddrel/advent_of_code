"""Advent of Code: day3, try to use closure and a decorator."""
def read_file(filename):
    """Read the text file in."""
    with open('data/{}.txt'.format(filename), 'r') as f:
        lines = f.readlines()
        lines = [line.strip('\n') for line in lines]
    return lines

def track_visits(func):
    def tracker(*args):
        log = (len(args[1]))
        if log % 1000 == 0:
            print("Delivered to {} houses".format(log))
        result = func(*args)
        return result
    return tracker

@track_visits
def update_log(coords, visited):
    """Parse the coords to a unique string and update the dict."""
    coords_name = str(coords[0]) + ',' + str(coords[1])
    if coords_name in visited:
        visited[coords_name] += 1
    else:
        visited[coords_name] = 1
    return visited

def update_flag(flag):
    """Binary switch"""
    if flag == 0:
        return 1
    if flag == 1:
        return 0

def main():
    """Wrap the main function."""
    print("Part I: ")
    lines = read_file('data3')[0]
    visited = {'0,0': 1}
    coords = [0, 0]  #x, y
    for movement in lines:
        if movement == 'v':
            coords[1] -= 1
        if movement == '^':
            coords[1] += 1
        if movement == '>':
            coords[0] += 1
        if movement == '<':
            coords[0] -= 1
        visited = update_log(coords, visited)
    print('Number of house with >= 1 present = ',len(visited))
    print('Part II: Double co-ords')
    visited = {'0,0': 2}
    coords_s = [0, 0]  #x, y
    coords_r = [0, 0]  #x, y
    paired = [coords_s, coords_r]
    alternate_flag = 0
    for movement in lines:
        if movement == 'v':
            paired[alternate_flag][1] -= 1
        if movement == '^':
            paired[alternate_flag][1] += 1
        if movement == '>':
            paired[alternate_flag][0] += 1
        if movement == '<':
            paired[alternate_flag][0] -= 1
        visited = update_log(paired[alternate_flag], visited)
        alternate_flag = update_flag(alternate_flag)
    print('Number of house with >= 1 present = ',len(visited))


if __name__ == '__main__':
    main()
