"""Advent of code Day 14 reipies."""

recipies = ['3', '7']
elf_a = 0
elf_b = 1
print(elf_a, elf_b)

while(len(recipies) < 503761 + 11):
    new_state = str(int(recipies[elf_a]) + int(recipies[elf_b]))
    for c in new_state:
        recipies.append(c)
    elf_a += (1 + int(recipies[elf_a]))
    elf_b += (1 + int(recipies[elf_b]))
    while elf_a > len(recipies) - 1:
        elf_a -= len(recipies)
    while elf_b > len(recipies) - 1:
        elf_b -= len(recipies)
# print(recipies)
print("".join(str(r) for r in recipies[503761:503761 + 10]))
